package com.msg.app.conversation.bean;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

@Table(value = "conversation")
public class ConversationEntity {

	@PrimaryKey(value = "id")
	private UUID id;

	@Column(value = "brand_email")
	private String brandEmail;

	@Column(value = "customer_email")
	private String customerEmail;

	@Column(value = "message")
	List<MessageType> message = new ArrayList<>();

	public UUID getId() {
		return id;
	}

	public void setId(final UUID id) {
		this.id = id;
	}

	public String getBrandEmail() {
		return brandEmail;
	}

	public void setBrandEmail(final String brandEmail) {
		this.brandEmail = brandEmail;
	}

	public String getCustomerEmail() {
		return customerEmail;
	}

	public void setCustomerEmail(final String customerEmail) {
		this.customerEmail = customerEmail;
	}

	public List<MessageType> getMessage() {
		return message;
	}

	public void setMessage(final List<MessageType> message) {
		this.message = message;
	}

}
