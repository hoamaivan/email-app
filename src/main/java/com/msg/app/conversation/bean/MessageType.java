package com.msg.app.conversation.bean;

import java.util.Date;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.UserDefinedType;

@UserDefinedType(value = "message")
public class MessageType {

	@Column(value = "sequence")
	private int sequence;

	@Column(value = "date_created")
	private Date dateCreated;

	@Column(value = "from_customer")
	private boolean fromCustomer;

	@Column(value = "subject")
	private String subject;

	@Column(value = "content")
	private String content;

	@Column(value = "agent_id")
	private String agentId;

	public int getSequence() {
		return sequence;
	}

	public void setSequence(final int sequence) {
		this.sequence = sequence;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(final Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public boolean isFromCustomer() {
		return fromCustomer;
	}

	public void setFromCustomer(final boolean fromCustomer) {
		this.fromCustomer = fromCustomer;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(final String subject) {
		this.subject = subject;
	}

	public String getContent() {
		return content;
	}

	public void setContent(final String content) {
		this.content = content;
	}

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(final String agentId) {
		this.agentId = agentId;
	}

}