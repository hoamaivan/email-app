package com.msg.app.conversation.dao;

import java.util.UUID;
import org.springframework.data.cassandra.repository.CassandraRepository;
import com.msg.app.conversation.bean.ConversationEntity;

public interface ConversationRepository extends CassandraRepository<ConversationEntity, UUID> {

}
