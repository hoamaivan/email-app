package com.msg.app.conversation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import com.msg.app.integrate.le.jws.JwsService;
import com.msg.app.integrate.le.jwt.JwtService;

@Service
public class HeaderInterceptor {

	@Autowired
	private JwtService jwtService;
	@Autowired
	private JwsService jwsService;

	public HttpHeaders buildHttpHeaders(final String extConsumerId) {
		final String jws = jwsService.getJwsWithRetry(extConsumerId);
		final String jwt = jwtService.getJwt();
		final HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.add("Authorization", jwt);
		headers.add("X-ON-BEHALF", jws);
		return headers;
	}
}
