package com.msg.app.conversation.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import javax.mail.MessagingException;
import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.msg.app.common.AppConstant;
import com.msg.app.common.exception.NotFoundException;
import com.msg.app.conversation.HeaderInterceptor;
import com.msg.app.conversation.bean.ConversationEntity;
import com.msg.app.conversation.bean.MessageType;
import com.msg.app.conversation.dao.ConversationRepository;
import com.msg.app.email.bean.Address;
import com.msg.app.email.bean.Mail;
import com.msg.app.email.sending.service.MailSendingService;
import com.msg.app.statistic.StatisticInMemory;
import com.msg.app.webhook.bean.ResponseContent;

@Service
public class ConversationServiceImpl implements ConversationService {

	static final Logger LOGGER = LoggerFactory.getLogger(ConversationServiceImpl.class);
	@Autowired
	private ConversationAPIService conversationAPIService;
	@Autowired
	private ConversationRepository conversationRepository;
	@Autowired
	private MessageService messageService;
	@Autowired
	ObjectMapper objectMapper;
	@Autowired
	HeaderInterceptor headerInterceptor;
	@Autowired
	MailSendingService mailSendingService;

	@Override
	public void createConversationAndSendMsg(final List<Mail> emails) {
		for (final Mail email : emails) {
			try {
				final SimpleDateFormat sdf = new SimpleDateFormat(AppConstant.YYYYMMDDHHMMSS);
				final String extConsumerId = sdf.format(new Date()) + "." + email.getFrom().getEmailAddress();
				final HttpHeaders httpHeaders = headerInterceptor.buildHttpHeaders(extConsumerId);
				final String conversationId = conversationAPIService.createNewConversation(httpHeaders, email);

				// Send message
				LOGGER.info("CREATED A NEW CONVERSATION | SENT MESSAGE {}", conversationId);

			} catch (final Exception e) {
				StatisticInMemory.addTotalMsgLEFail(email.getTo().getEmailAddress(), 1);
				LOGGER.error("Creating and sending message error {}", email.getFrom().getEmailAddress());
			}
		}
	}

	@Override
	public void respondToCustomer(final ResponseContent resContent) throws MessagingException {
		final String conversationId = resContent.getBody().getChanges().get(0).getDialogId();
		final Optional<ConversationEntity> convEntity = conversationRepository
				.findById(UUID.fromString(conversationId));
		if (convEntity.isPresent()) {
			try {
				final Mail email = createResponseEmail(convEntity.get(), resContent);
				mailSendingService.sendMail(email, createConversationHistory(convEntity.get()));

				convEntity.get().getMessage().add(createAgentMessage(email, resContent));
				conversationRepository.save(convEntity.get());

				StatisticInMemory.addTotalMailSmtp(convEntity.get().getBrandEmail(), 1);

				final HttpHeaders httpHeaders = headerInterceptor
						.buildHttpHeaders(convEntity.get().getJwsExtConsumerId());
				conversationAPIService.closeConversation(httpHeaders, conversationId);
			} catch (final HttpClientErrorException ex) {
			} catch (final MessagingException ex) {
				throw ex;
			}
		} else {
			LOGGER.error("Not found conversation: {}", conversationId);
			throw new NotFoundException("Not found conversation by id");
		}
	}

	private ConversationEntity createConversation(final String conversationId, final String extConsumerId,
			final Mail email) {
		final ConversationEntity convEntity = new ConversationEntity();
		convEntity.setId(UUID.fromString(conversationId));
		convEntity.setCustomerEmail(email.getFrom().getEmailAddress());
		convEntity.setBrandEmail(email.getTo().getEmailAddress());
		convEntity.setJwsExtConsumerId(extConsumerId);
		return convEntity;
	}

	private MessageType createCustomerMessage(final Mail email) {
		final MessageType message = new MessageType();
		message.setSequence(1);
		message.setDateCreated(new Date());
		message.setFromCustomer(true);
		message.setSubject(email.getSubject());
		message.setContent(email.getContent());
		message.setAgentId("");
		return message;
	}

	private Mail createResponseEmail(final ConversationEntity convEntity, final ResponseContent resContent) {
		final Address addressFrom = new Address(convEntity.getBrandEmail(), "");
		final Address addressTo = new Address(convEntity.getCustomerEmail(), "");
		String subject = convEntity.getMessage().get(0).getSubject();
		if (subject != null && !subject.regionMatches(true, 0, "Re: ", 0, 4)) {
			subject = "Re: " + subject;
		}
		final String content = resContent.getBody().getChanges().get(0).getEvent().getMessage();
		return new Mail(null, addressFrom, addressTo, new Date(), subject, content);
	}

	private MessageType createAgentMessage(final Mail email, final ResponseContent resContent) {
		final MessageType msgType = new MessageType();
		msgType.setSequence(resContent.getBody().getChanges().get(0).getSequence());
		msgType.setDateCreated(new Date());
		msgType.setFromCustomer(false);
		msgType.setSubject(email.getSubject());
		msgType.setContent(resContent.getBody().getChanges().get(0).getEvent().getMessage());
		msgType.setAgentId(resContent.getBody().getChanges().get(0).getOriginatorMetadata().getId());
		return msgType;
	}

	private String createConversationHistory(final ConversationEntity convEntity) {
		final String historyTemplate = "<div>On %s at %s %s wrote:<br/>%s</div>";
		final SimpleDateFormat dateFormater = new SimpleDateFormat("EEE, MMM dd, yyyy");
		final SimpleDateFormat timeFormatter = new SimpleDateFormat("HH:mm:ss");
		if (convEntity.getMessage().isEmpty()) {
			return Strings.EMPTY;
		}
		final StringBuilder messageHistory = new StringBuilder(Strings.EMPTY);

		for (final MessageType message : convEntity.getMessage()) {
			final String messageOwner = message.isFromCustomer() ? convEntity.getCustomerEmail()
					: convEntity.getBrandEmail();
			messageHistory.append(String.format(historyTemplate, dateFormater.format(message.getDateCreated()),
					timeFormatter.format(message.getDateCreated()), messageOwner, message.getContent()));
			messageHistory.append("<br/>");
		}

		return messageHistory.toString();
	}

}
