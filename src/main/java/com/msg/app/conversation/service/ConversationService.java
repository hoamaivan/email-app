package com.msg.app.conversation.service;

import java.util.List;
import javax.mail.MessagingException;
import com.msg.app.email.bean.Mail;
import com.msg.app.webhook.bean.ResponseContent;

public interface ConversationService {

	public void createConversationAndSendMsg(List<Mail> emails);

	public void respondToCustomer(ResponseContent resContent) throws MessagingException;
}
