package com.msg.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmailApp {

	public static void main(final String[] args) {
		SpringApplication.run(EmailApp.class, args);
	}

}
