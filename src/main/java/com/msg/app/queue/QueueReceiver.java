package com.msg.app.queue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.msg.app.account.bean.BrandAccount;
import com.msg.app.account.scheduler.InternalQueue;
import com.msg.app.config.QueueConfig;
import com.msg.app.conversation.service.ConversationService;
import com.msg.app.email.pulling.service.PullingService;
import com.msg.app.webhook.bean.ResponseContent;

@Component
public class QueueReceiver {

	static final Logger LOGGER = LoggerFactory.getLogger(QueueReceiver.class);

	@Autowired
	private PullingService emailPullingService;
	@Autowired
	private ConversationService conversationService;

	@RabbitListener(queues = QueueConfig.INCOMING_QUEUE_NAME)
	public void handleMessage(final BrandAccount brandAccount) {
		if (brandAccount == null || InternalQueue.containsBrandAccountInProcess(brandAccount.getBrandEmail())) {
			return;
		}
		final String emailBrand = brandAccount.getBrandEmail();
		InternalQueue.addBrandAccountInProcess(emailBrand);
		try {
			emailPullingService.pullEmailFromSMTP(brandAccount);
		} catch (final Exception ex) {
			LOGGER.error("ERROR: Flow Pull Email and Migrate - SMTP {} - {}",
					brandAccount.getBrandName(), ex.getMessage());
		}
		InternalQueue.removeBrandAccountInProcess(emailBrand);
	}

	@RabbitListener(queues = QueueConfig.OUTGOING_QUEUE_NAME)
	public void handleMessage(final ResponseContent resContent) {
		if (resContent == null) {
			return;
		}
		final String dialogId = resContent.getBody().getChanges().get(0).getDialogId();
		try {
			conversationService.respondToCustomer(resContent);
			LOGGER.info("Responded To Customer - DialogId {}", dialogId);
		} catch (final Exception ex) {
			LOGGER.error("ERROR: Flow Agent Respond To Customer: DialogId {}", dialogId);
		}
	}
}
