package com.msg.app.queue;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.msg.app.account.bean.BrandAccount;
import com.msg.app.config.QueueConfig;
import com.msg.app.webhook.bean.ResponseContent;

@Service
public class QueueSender {

	@Autowired
	private final RabbitTemplate rabbitTemplate;

	@Autowired
	public QueueSender(final RabbitTemplate rabbitTemplate) {
		this.rabbitTemplate = rabbitTemplate;
	}

	public void send(final BrandAccount brandAccount) {
		this.rabbitTemplate.convertAndSend(QueueConfig.EXCHANGE_NAME, QueueConfig.INCOMING_ROUTING_KEY, brandAccount);
	}

	public void sendMessageToQueue(final ResponseContent responseContent) {
		this.rabbitTemplate.convertAndSend(QueueConfig.EXCHANGE_NAME, QueueConfig.OUTGOING_ROUTING_KEY,
				responseContent);
	}
}
