package com.msg.app.statistic;

import org.springframework.data.cassandra.repository.CassandraRepository;
import com.msg.app.statistic.bean.StatisticEntity;
import com.msg.app.statistic.bean.StatisticPrimaryKey;

public interface StatisticRepository extends CassandraRepository<StatisticEntity, StatisticPrimaryKey> {

}
