package com.msg.app.statistic.bean;

public class Statistic {

	private String brandEmail;
	private Integer totalMailPulling;
	private Integer totalMsgLESuccess;
	private Integer totalMsgLEFail;
	private Integer totalMsgAgent;
	private Integer totalMailSmtp;

	public Statistic(final String brandEmail, final Integer totalMailPulling, final Integer totalMsgLESuccess,
			final Integer totalMsgLEFail, final Integer totalMsgAgent, final Integer totalMailSmtp) {
		this.brandEmail = brandEmail;
		this.totalMailPulling = totalMailPulling;
		this.totalMsgLESuccess = totalMsgLESuccess;
		this.totalMsgLEFail = totalMsgLEFail;
		this.totalMsgAgent = totalMsgAgent;
		this.totalMailSmtp = totalMailSmtp;
	}

	public Statistic() {
	}

	public String getBrandEmail() {
		return brandEmail;
	}

	public void setBrandEmail(final String brandEmail) {
		this.brandEmail = brandEmail;
	}

	public Integer getTotalMailPulling() {
		return totalMailPulling;
	}

	public void setTotalMailPulling(final Integer totalMailPulling) {
		this.totalMailPulling = totalMailPulling;
	}

	public Integer getTotalMailToLESuccess() {
		return totalMsgLESuccess;
	}

	public void setTotalMsgLESuccess(final Integer totalMsgLESuccess) {
		this.totalMsgLESuccess = totalMsgLESuccess;
	}

	public Integer getTotalMsgLEFail() {
		return totalMsgLEFail;
	}

	public void setTotalMsgLEFail(final Integer totalMsgLEFail) {
		this.totalMsgLEFail = totalMsgLEFail;
	}

	public Integer getTotalMsgAgent() {
		return totalMsgAgent;
	}

	public void setTotalMsgAgent(final Integer totalMsgAgent) {
		this.totalMsgAgent = totalMsgAgent;
	}

	public Integer getTotalMailSmtp() {
		return totalMailSmtp;
	}

	public void setTotalMailSmtp(final Integer totalMailSmtp) {
		this.totalMailSmtp = totalMailSmtp;
	}

}
