package com.msg.app.statistic.bean;

import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

@Table(value = "brand_statistic")
public class StatisticEntity {

	@PrimaryKey
	private StatisticPrimaryKey primaryKey;

	@Column("total_mail_pulling")
	private Integer totalMailPulling;

	@Column("total_msg_le_success")
	private Integer totalMsgLESuccess;

	@Column("total_msg_le_fail")
	private Integer totalMsgLEFail;

	@Column("total_msg_agent")
	private Integer totalMsgAgent;

	@Column(value = "total_mail_smtp")
	private Integer totalMailSmtp;

	public StatisticEntity() {
	}

	public StatisticEntity(final StatisticPrimaryKey primaryKey, final Integer totalMailPulling,
			final Integer totalMsgLESuccess, final Integer totalMsgLEFail, final Integer totalMsgAgent,
			final Integer totalMailSmtp) {
		this.primaryKey = primaryKey;
		this.totalMailPulling = totalMailPulling;
		this.totalMsgLESuccess = totalMsgLESuccess;
		this.totalMsgLEFail = totalMsgLEFail;
		this.totalMsgAgent = totalMsgAgent;
		this.totalMailSmtp = totalMailSmtp;
	}

	public StatisticPrimaryKey getPrimaryKey() {
		return primaryKey;
	}

	public void setPrimaryKey(final StatisticPrimaryKey primaryKey) {
		this.primaryKey = primaryKey;
	}

	public Integer getTotalMailPulling() {
		return totalMailPulling;
	}

	public void setTotalMailPulling(final Integer totalMailPulling) {
		this.totalMailPulling = totalMailPulling;
	}

	public Integer getTotalMsgLESuccess() {
		return totalMsgLESuccess;
	}

	public void setTotalMsgLESuccess(final Integer totalMsgLESuccess) {
		this.totalMsgLESuccess = totalMsgLESuccess;
	}

	public Integer getTotalMsgLEFail() {
		return totalMsgLEFail;
	}

	public void setTotalMsgLEFail(final Integer totalMsgLEFail) {
		this.totalMsgLEFail = totalMsgLEFail;
	}

	public Integer getTotalMsgAgent() {
		return totalMsgAgent;
	}

	public void setTotalMsgAgent(final Integer totalMsgAgent) {
		this.totalMsgAgent = totalMsgAgent;
	}

	public Integer getTotalMailSmtp() {
		return totalMailSmtp;
	}

	public void setTotalMailSmtp(final Integer totalMailSmtp) {
		this.totalMailSmtp = totalMailSmtp;
	}

}
