package com.msg.app.statistic.bean;

import java.io.Serializable;
import java.util.Date;
import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyClass;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;

@PrimaryKeyClass
public class StatisticPrimaryKey implements Serializable {

	private static final long serialVersionUID = 8252609795274792780L;

	@PrimaryKeyColumn(name = "brand_email", type = PrimaryKeyType.PARTITIONED)
	private String brandEmail;

	@PrimaryKeyColumn(name = "date_created", type = PrimaryKeyType.PARTITIONED)
	private Date dateCreated;

	public StatisticPrimaryKey() {
	}

	public StatisticPrimaryKey(final String brandEmail, final Date dateCreated) {
		this.brandEmail = brandEmail;
		this.dateCreated = dateCreated;
	}

	public String getBrandEmail() {
		return brandEmail;
	}

	public void setBrandEmail(final String brandEmail) {
		this.brandEmail = brandEmail;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(final Date dateCreated) {
		this.dateCreated = dateCreated;
	}

}
