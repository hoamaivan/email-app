package com.msg.app.statistic.scheduler;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;

@Configuration
@ConfigurationProperties(prefix = "email-app.statistic.scheduler.job-detail")
public class JobSaveStatisticConfig {

	private Long startDelay;

	private String cronExpression;

	private String description;

	private String jobName;

	@Bean(name = "cronTriggerSavingStatisticBean")
	public CronTriggerFactoryBean cronPushAccountTriggerBean() {
		final CronTriggerFactoryBean stFactory = new CronTriggerFactoryBean();
		stFactory.setJobDetail(jobDetailSavingStatistic().getObject());
		stFactory.setStartDelay(startDelay);
		stFactory.setCronExpression(cronExpression);
		return stFactory;
	}

	@Bean
	public JobDetailFactoryBean jobDetailSavingStatistic() {
		final JobDetailFactoryBean jobDetailFactoryBean = new JobDetailFactoryBean();
		jobDetailFactoryBean.setJobClass(JobSavingStatisticExecute.class);
		jobDetailFactoryBean.setDescription(description);
		jobDetailFactoryBean.setDurability(true);
		jobDetailFactoryBean.setName(jobName);

		return jobDetailFactoryBean;
	}

	public Long getStartDelay() {
		return startDelay;
	}

	public void setStartDelay(final Long startDelay) {
		this.startDelay = startDelay;
	}

	public String getCronExpression() {
		return cronExpression;
	}

	public void setCronExpression(final String cronExpression) {
		this.cronExpression = cronExpression;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(final String jobName) {
		this.jobName = jobName;
	}

}
