package com.msg.app.statistic.scheduler;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import com.msg.app.statistic.StatisticInMemory;
import com.msg.app.statistic.StatisticRepository;
import com.msg.app.statistic.bean.Statistic;
import com.msg.app.statistic.bean.StatisticEntity;
import com.msg.app.statistic.bean.StatisticPrimaryKey;

public class JobSavingStatisticExecute implements Job {

	@Autowired
	private StatisticRepository statisticRepository;

	@Override
	public void execute(final JobExecutionContext context) throws JobExecutionException {
		final List<Statistic> brandStatistic = StatisticInMemory.getAllBrandStatistic();
		statisticRepository
				.saveAll(brandStatistic.stream().map(this::createStatisticEntity).collect(Collectors.toList()));
		StatisticInMemory.removeAllBrandStatistic();
	}

	private StatisticEntity createStatisticEntity(final Statistic brandStatistic) {
		final StatisticPrimaryKey primaryKey = new StatisticPrimaryKey(brandStatistic.getBrandEmail(), new Date());
		return new StatisticEntity(primaryKey, brandStatistic.getTotalMailPulling(),
				brandStatistic.getTotalMailToLESuccess(), brandStatistic.getTotalMsgLEFail(),
				brandStatistic.getTotalMsgAgent(), brandStatistic.getTotalMailSmtp());
	}
}
