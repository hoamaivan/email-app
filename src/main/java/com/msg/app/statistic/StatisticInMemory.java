package com.msg.app.statistic;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import com.msg.app.statistic.bean.Statistic;

public class StatisticInMemory {

	private static Map<String, Statistic> brandStatistices = new ConcurrentHashMap<>();

	private StatisticInMemory() {
	}

	public static boolean brandStatistic(final String brandEmail) {
		return brandStatistices.containsKey(brandEmail);
	}

	public static Statistic getBrandStatistic(final String brandEmail) {
		return brandStatistic(brandEmail) ? brandStatistices.get(brandEmail) : new Statistic(brandEmail, 0, 0, 0, 0, 0);
	}

	public static void removeAllBrandStatistic() {
		StatisticInMemory.brandStatistices.clear();
	}

	public static List<Statistic> getAllBrandStatistic() {
		return brandStatistices.values().stream().collect(Collectors.toList());
	}

	public static void addBrandStatistic(final Statistic brandStatistic) {
		brandStatistices.put(brandStatistic.getBrandEmail(), brandStatistic);
	}

	public static void addTotalMailPulling(final String brandEmail, final Integer totalMailPulled) {
		final Statistic brandStatistic = StatisticInMemory.getBrandStatistic(brandEmail);
		brandStatistic.setTotalMailPulling(totalMailPulled + brandStatistic.getTotalMailPulling());
		StatisticInMemory.addBrandStatistic(brandStatistic);
	}

	public static void addTotalMsgLESuccess(final String brandEmail, final Integer totalMsgLESuccess) {
		final Statistic brandStatistic = StatisticInMemory.getBrandStatistic(brandEmail);
		brandStatistic.setTotalMsgLESuccess(totalMsgLESuccess + brandStatistic.getTotalMailToLESuccess());
		StatisticInMemory.addBrandStatistic(brandStatistic);
	}

	public static void addTotalMsgLEFail(final String brandEmail, final Integer totalMsgLEFail) {
		final Statistic brandStatistic = StatisticInMemory.getBrandStatistic(brandEmail);
		brandStatistic.setTotalMsgLEFail(totalMsgLEFail + brandStatistic.getTotalMsgLEFail());
		StatisticInMemory.addBrandStatistic(brandStatistic);
	}

	public static void addTotalMsgAgent(final String brandEmail, final Integer totalMsgAgent) {
		final Statistic brandStatistic = StatisticInMemory.getBrandStatistic(brandEmail);
		brandStatistic.setTotalMsgAgent(totalMsgAgent + brandStatistic.getTotalMsgAgent());
		StatisticInMemory.addBrandStatistic(brandStatistic);
	}

	public static void addTotalMailSmtp(final String brandEmail, final Integer totalMailSmtp) {
		final Statistic brandStatistic = StatisticInMemory.getBrandStatistic(brandEmail);
		brandStatistic.setTotalMailSmtp(totalMailSmtp + brandStatistic.getTotalMailSmtp());
		StatisticInMemory.addBrandStatistic(brandStatistic);
	}
}
