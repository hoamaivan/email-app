package com.msg.app.email.sending.service;

import java.util.Properties;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.msg.app.account.bean.BrandAccount;
import com.msg.app.account.service.BrandAccountService;
import com.msg.app.common.exception.InvalidParameterException;
import com.msg.app.config.SendingMailConfig;
import com.msg.app.email.bean.Mail;
import com.msg.app.email.template.TemplateService;

@Service
public class MailSendingServiceImpl implements MailSendingService {

	static final Logger LOGGER = LoggerFactory.getLogger(MailSendingServiceImpl.class);

	@Autowired
	private BrandAccountService brandAccountService;

	@Autowired
	private TemplateService templateService;

	@Autowired
	private SendingMailConfig sendingMailConfig;

	@Override
	public void sendMail(final Mail mail, final String messageHistory) throws MessagingException {
		final BrandAccount account = brandAccountService.findByBrandEmail(mail.getFrom().getEmailAddress())
				.orElseThrow(() -> new InvalidParameterException("Invalid brand account"));

		final JavaMailSender javaMailSender = createMailSender(account);
		final MimeMessage mimeMessage = javaMailSender.createMimeMessage();
		final MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true, "UTF-8");
		helper.setFrom(mail.getFrom().getEmailAddress());
		helper.setTo(mail.getTo().getEmailAddress());
		helper.setSubject(mail.getSubject());

		String body = templateService.buildResponseTemplate(account.getTemplate().getHeader(), mail.getContent(),
				account.getTemplate().getBodyStyle(), account.getTemplate().getMessagingChannel(),
				account.getTemplate().getFooter(), messageHistory);

		if (body == null) {
			body = "";
		}

		helper.setText(body, true);
		javaMailSender.send(mimeMessage);
	}

	private JavaMailSender createMailSender(final BrandAccount account) {
		final JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
		mailSender.setHost(account.getOutgoing().getHost());
		mailSender.setPort(account.getOutgoing().getPort());
		mailSender.setUsername(account.getBrandEmail());
		mailSender.setPassword(account.getSmtpPassword());
		mailSender.setProtocol(account.getOutgoing().getProtocol().toLowerCase());
		final Properties properties = new Properties();
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.smtp.ssl.enable", account.getOutgoing().isRequireSSL());
		properties.put("mail.smtp.connectiontimeout", sendingMailConfig.getConnectionTimeout());

		mailSender.setJavaMailProperties(properties);
		return mailSender;
	}
}
