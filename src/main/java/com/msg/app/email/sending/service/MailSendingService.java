package com.msg.app.email.sending.service;

import javax.mail.MessagingException;
import com.msg.app.email.bean.Mail;

public interface MailSendingService {

	void sendMail(Mail mail, String messageHistory) throws MessagingException;
}
