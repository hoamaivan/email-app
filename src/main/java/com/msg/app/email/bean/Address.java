package com.msg.app.email.bean;

public class Address {

	private String emailAddress;
	private String name;

	public Address() {
	}

	public Address(final String emailAddress, final String name) {
		this.emailAddress = emailAddress;
		this.name = name;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(final String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

}
