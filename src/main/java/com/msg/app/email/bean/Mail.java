package com.msg.app.email.bean;

import java.util.Date;

public class Mail {

	private String messageId;
	private Address from;
	private Address to;
	private Date sentDate;
	private String subject;
	private String content;

	public Mail() {
	}

	public Mail(final String messageId, final Address from, final Address to, final Date sentDate, final String subject,
			final String content) {
		this.messageId = messageId;
		this.from = from;
		this.to = to;
		this.sentDate = sentDate;
		this.subject = subject;
		this.content = content;
	}

	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(final String messageId) {
		this.messageId = messageId;
	}

	public Address getFrom() {
		return from;
	}

	public void setFrom(final Address from) {
		this.from = from;
	}

	public Address getTo() {
		return to;
	}

	public void setTo(final Address to) {
		this.to = to;
	}

	public Date getSentDate() {
		return sentDate;
	}

	public void setSentDate(final Date sentDate) {
		this.sentDate = sentDate;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(final String subject) {
		this.subject = subject;
	}

	public String getContent() {
		return content;
	}

	public void setContent(final String content) {
		this.content = content;
	}
}
