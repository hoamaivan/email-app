package com.msg.app.email.pulling.service;

import java.io.IOException;
import javax.mail.MessagingException;
import com.msg.app.account.bean.BrandAccount;

public interface PullingService {

	public void pullEmailFromSMTP(BrandAccount brandAccount) throws MessagingException, IOException;

}
