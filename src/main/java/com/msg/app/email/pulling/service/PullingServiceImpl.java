package com.msg.app.email.pulling.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.search.AndTerm;
import javax.mail.search.ComparisonTerm;
import javax.mail.search.FlagTerm;
import javax.mail.search.ReceivedDateTerm;
import javax.mail.search.SearchTerm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.msg.app.account.bean.BrandAccount;
import com.msg.app.conversation.service.ConversationService;
import com.msg.app.email.bean.Mail;
import com.msg.app.email.parsing.service.ParsingService;
import com.msg.app.statistic.StatisticInMemory;

@Service
public class PullingServiceImpl implements PullingService {

	static final Logger LOGGER = LoggerFactory.getLogger(PullingServiceImpl.class);
	@Autowired
	ParsingService parsingService;
	@Autowired
	ConversationService conversationService;

	@Value("${email-app.email.pulling.limit-messages-per-execution}")
	private int batchNumber;
	@Value("${email-app.email.pulling.connection-timeout}")
	private int connectionTimeout;

	private Session buildSession(final BrandAccount brandAccount) {
		String protocol = brandAccount.getIncoming().getProtocol().toLowerCase();
		if ("pop3".equals(protocol)) {
			protocol = brandAccount.getIncoming().isRequireSSL() ? "pop3s" : "pop3";
		} else if ("imap".equals(protocol)) {
			protocol = brandAccount.getIncoming().isRequireSSL() ? "imaps" : "imap";
		}

		final Properties props = new Properties();
		props.put("mail.user", brandAccount.getBrandEmail());
		props.put("mail.password", brandAccount.getSmtpPassword());
		props.put("mail.host", brandAccount.getIncoming().getHost());
		props.put("mail." + protocol + ".port", brandAccount.getIncoming().getPort());
		props.put("mail.debug", "false");
		props.put("mail.store.protocol", protocol);
		props.put("mail.transport.protocol", "smtp");
		props.put("mail." + protocol + ".connectiontimeout", connectionTimeout);

		return Session.getInstance(props, new javax.mail.Authenticator() {

			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(brandAccount.getBrandEmail(), brandAccount.getSmtpPassword());
			}
		});
	}

	@Override
	public void pullEmailFromSMTP(final BrandAccount brandAccount) throws MessagingException, IOException {
		final Session session = buildSession(brandAccount);
		final Store store = session.getStore();
		Folder root = null;
		Folder inbox = null;

		store.connect();
		root = store.getDefaultFolder();
		inbox = root.getFolder("inbox");
		inbox.open(Folder.READ_WRITE);
		List<Mail> emails = new ArrayList<>();
		Message[] msgs = null;

		if (brandAccount.getIncoming().getProtocol().equalsIgnoreCase("pop3")) { // POP3
			msgs = getMessagePOP3(inbox, brandAccount.getDateCreated());
		} else { // IMAP
			msgs = getMessageIMAP(inbox, brandAccount.getDateCreated());
		}

		if (msgs != null && msgs.length > 0) {

			StatisticInMemory.addTotalMailPulling(brandAccount.getBrandEmail(), msgs.length);
			emails = parsingService.parseEmailContent(msgs, brandAccount.getBrandEmail());
		}

		markReadMessages(msgs, brandAccount.getIncoming().getProtocol().toLowerCase());
		inbox.close(true);
		store.close();
		if (!emails.isEmpty()) {
			conversationService.createConversationAndSendMsg(emails);
		}
	}

	private Message[] getMessagePOP3(final Folder inbox, final Date beginDate) throws MessagingException {
		final Message[] msgs = inbox.getMessages();
		final List<Message> filterdMessage = new ArrayList<>();
		for (final Message msg : msgs) {
			if (filterdMessage.size() == batchNumber) {
				break;
			}
			if (msg.getSentDate() != null && msg.getSentDate().after(beginDate)) {
				filterdMessage.add(msg);
			}
		}
		return filterdMessage.toArray(new Message[filterdMessage.size()]);
	}

	private Message[] getMessageIMAP(final Folder inbox, final Date beginDate) throws MessagingException {
		final ReceivedDateTerm beginDateTerm = new ReceivedDateTerm(ComparisonTerm.GE, beginDate);
		// Fetch unseen messages
		final SearchTerm term = new AndTerm(beginDateTerm, new FlagTerm(new Flags(Flags.Flag.SEEN), false));
		Message[] msgs = inbox.search(term);
		if (msgs != null && msgs.length > 0) {
			msgs = Arrays.copyOfRange(msgs, 0, msgs.length > batchNumber ? (batchNumber) : msgs.length);
		}
		return msgs;
	}

	private void markReadMessages(final Message[] messages, final String protocol) throws MessagingException {
		if ("pop3".equals(protocol)) {
			for (final Message message : messages) {
				message.setFlag(Flags.Flag.DELETED, true);
			}
		} else { // IMAP
			for (final Message message : messages) {
				message.setFlag(Flags.Flag.SEEN, true);
			}
		}
	}
}
