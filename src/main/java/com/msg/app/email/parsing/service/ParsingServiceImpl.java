package com.msg.app.email.parsing.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.internet.InternetAddress;
import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import com.msg.app.email.bean.Address;
import com.msg.app.email.bean.Mail;
import com.msg.app.statistic.StatisticInMemory;

/**
 * Created on 2-Apr-2019.
 */
@Service
public class ParsingServiceImpl implements ParsingService {

	static final Logger LOGGER = LoggerFactory.getLogger(ParsingServiceImpl.class);

	@Override
	public List<Mail> parseEmailContent(final Message[] messages, final String toEmail) {
		final List<Mail> emails = new ArrayList<>();
		for (final Message message : messages) {
			try {
				emails.add(messageToEmailDTO(message, toEmail));
			} catch (final Exception e) {
				StatisticInMemory.addTotalMsgLEFail(toEmail, 1);
				LOGGER.error("Parsing email error of brand account : {}", toEmail);
			}
		}
		return emails;
	}

	private Mail messageToEmailDTO(final Message message, final String toEmail) throws MessagingException, IOException {
		final Mail email = new Mail();
		final InternetAddress internetAddress = ((InternetAddress) message.getFrom()[0]);
		final Address addressFrom = new Address(internetAddress.getAddress(),
				internetAddress.gethumanal() != null ? internetAddress.gethumanal() : Strings.EMPTY);
		email.setFrom(addressFrom);
		email.setTo(new Address(toEmail, Strings.EMPTY));
		email.setSentDate(message.getSentDate());
		email.setMessageId(message.getHeader("Message-ID")[0]);
		email.setSubject(message.getSubject());
		email.setContent(getTextPlain(message.getContent()));
		return email;
	}

	private String getTextPlain(final Object messageContent) throws MessagingException, IOException {
		final StringBuilder content = new StringBuilder("");
		if (messageContent instanceof Multipart) {
			final Multipart multipart = (Multipart) messageContent;
			final int partCount = multipart.getCount();
			for (int i = 0; i < partCount; i++) {
				final BodyPart bodyPart = multipart.getBodyPart(i);
				if (bodyPart.isMimeType("text/plain")) {
					content.append(bodyPart.getContent().toString());
				} else if (bodyPart.isMimeType("multipart/*")) {
					content.append(getTextPlain(bodyPart.getContent()));
				}
			}
		} else {
			content.append(messageContent.toString());
		}
		return content.toString();
	}

}
