package com.msg.app.email.parsing.service;

import java.util.List;
import javax.mail.Message;
import com.msg.app.email.bean.Mail;

public interface ParsingService {

	public List<Mail> parseEmailContent(Message[] messages, String toEmail);

}
