package com.msg.app.common.exception;

public class InvalidParameterException extends RuntimeException {

	private static final long serialVersionUID = 7168136064844202277L;
	private static final String DEFAULT_MESSAGE = "Parameters invalid";

	public InvalidParameterException() {
		super(DEFAULT_MESSAGE);
	}

	public InvalidParameterException(final String message) {
		super(message);
	}
}
