package com.msg.app.common.exception;

public class PasswordEncryptException extends RuntimeException {

	private static final long serialVersionUID = 8776711877773665012L;

	private static final String DEFAULT_MESSAGE = "Encrypt password fail";

	public PasswordEncryptException() {
		super(DEFAULT_MESSAGE);
	}

	public PasswordEncryptException(final String message) {
		super(message);
	}
}
