package com.msg.app.common.exception;

public class PasswordDecryptException extends RuntimeException {

	private static final long serialVersionUID = -3754551051310145617L;
	private static final String DEFAULT_MESSAGE = "Decrypt password fail";

	public PasswordDecryptException() {
		super(DEFAULT_MESSAGE);
	}

	public PasswordDecryptException(final String message) {
		super(message);
	}
}
