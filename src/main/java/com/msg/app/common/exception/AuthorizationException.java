package com.msg.app.common.exception;

public class AuthorizationException extends RuntimeException {

	private static final long serialVersionUID = -5721184150820364828L;

	private static final String DEFAULT_MESSAGE = "Authorization error";

	public AuthorizationException() {
		super(DEFAULT_MESSAGE);
	}

	public AuthorizationException(final String message) {
		super(message);
	}

}
