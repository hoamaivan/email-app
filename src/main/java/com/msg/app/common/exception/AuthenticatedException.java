package com.msg.app.common.exception;

public class AuthenticatedException extends RuntimeException {

	private static final long serialVersionUID = 2422234780110929698L;

	private static final String DEFAULT_MESSAGE = "Authenticated error";

	public AuthenticatedException() {
		super(DEFAULT_MESSAGE);
	}

	public AuthenticatedException(final String message) {
		super(message);
	}
}
