package com.msg.app.common.exception;

public class NotFoundException extends RuntimeException {

	private static final long serialVersionUID = 982694052097534734L;
	private static final String DEFAULT_MESSAGE = "Cannot found data";

	public NotFoundException() {
		super(DEFAULT_MESSAGE);
	}

	public NotFoundException(final String message) {
		super(message);
	}
}
