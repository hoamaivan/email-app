package com.msg.app.common.exception;

public class FileTypeException extends RuntimeException {

	private static final long serialVersionUID = 982694052097534734L;
	private static final String DEFAULT_MESSAGE = "Invalid Content Type";

	public FileTypeException() {
		super(DEFAULT_MESSAGE);
	}

	public FileTypeException(final String message) {
		super(message);
	}
}
