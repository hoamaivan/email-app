package com.msg.app.common;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import com.msg.app.common.exception.AuthenticatedException;
import com.msg.app.common.exception.AuthorizationException;
import com.msg.app.common.exception.FileTypeException;
import com.msg.app.common.exception.InvalidParameterException;

@ControllerAdvice
public class APIExceptionHandler extends ResponseEntityExceptionHandler {

	private static final Logger LOG = LoggerFactory.getLogger(APIExceptionHandler.class);

	@ExceptionHandler
	@ResponseBody
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public Object handleConstraintViolationException(final AuthorizationException e, final HttpServletRequest request,
			final HttpServletResponse response) {
		LOG.error(e.getMessage());
		return e.getMessage();
	}

	@ExceptionHandler
	@ResponseBody
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public Object handleEPGTrxSearchException(final AuthenticatedException e, final HttpServletRequest request,
			final HttpServletResponse response) {
		LOG.error(e.getMessage());
		return e.getMessage();
	}

	@ExceptionHandler
	@ResponseBody
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public Object handleEPGTrxSearchException(final FileTypeException e, final HttpServletRequest request,
			final HttpServletResponse response) {
		LOG.error(e.getMessage());
		return e.getMessage();
	}

	@ExceptionHandler
	@ResponseBody
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public Object handleEPGInvalidTrxSearchException(final InvalidParameterException e,
			final HttpServletRequest request, final HttpServletResponse response) {
		LOG.error(e.getMessage());
		return e.getMessage();
	}

	@ExceptionHandler
	@ResponseBody
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public Object handleEPGOutOfLimitDataException(final NotFoundException e, final HttpServletRequest request,
			final HttpServletResponse response) {
		LOG.error(e.getMessage());
		return e.getMessage();
	}

}
