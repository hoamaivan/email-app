package com.msg.app.common;

public class AppConstant {

	private AppConstant() {
	}

	public static final int MAXLENGTH_MESSAGE = 8000;

	public static final String YYYYMMDDHHMMSS = "yyyyMMddHHmmss";
}
