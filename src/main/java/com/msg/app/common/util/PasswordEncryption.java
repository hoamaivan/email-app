package com.msg.app.common.util;

import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Base64;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import com.msg.app.common.exception.PasswordDecryptException;
import com.msg.app.common.exception.PasswordEncryptException;

@Component
public class PasswordEncryption {

	@Value("${email-app.password.salt}")
	private String salt;

	@Value("${email-app.password.secret-key}")
	private String key;

	public String encrypt(final String originalPassword) {
		return "encryptPassword";
	}

	public String decrypt(final String passwordDecryption) {
		return "decryptPassword";
	}
}
