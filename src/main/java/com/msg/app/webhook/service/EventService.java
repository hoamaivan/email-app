package com.msg.app.webhook.service;

import org.springframework.http.HttpStatus;
import com.msg.app.webhook.bean.ResponseContent;

public interface EventService {

	HttpStatus sendMessageToQueue(ResponseContent responseContent);
}
