package com.msg.app.webhook.service;

import java.util.UUID;
import org.springframework.amqp.AmqpException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import com.msg.app.common.exception.NotFoundException;
import com.msg.app.conversation.bean.ConversationEntity;
import com.msg.app.conversation.dao.ConversationRepository;
import com.msg.app.queue.QueueSender;
import com.msg.app.statistic.StatisticInMemory;
import com.msg.app.webhook.bean.ResponseContent;

@Service
public class EventServiceImpl implements EventService {

	@Autowired
	private QueueSender queueSender;

	@Autowired
	private ConversationRepository conversationRepository;

	@Override
	public HttpStatus sendMessageToQueue(final ResponseContent responseContent) {
		try {

			final ConversationEntity conversation = conversationRepository
					.findById(UUID.fromString(responseContent.getBody().getChanges().get(0).getDialogId()))
					.orElseThrow(() -> new NotFoundException("Conversation not found"));
			StatisticInMemory.addTotalMsgAgent(conversation.getBrandEmail(), 1);

			queueSender.sendMessageToQueue(responseContent);
			return HttpStatus.OK;
		} catch (final AmqpException ex) {
			return HttpStatus.INTERNAL_SERVER_ERROR;
		} catch (final NotFoundException ex) {
			return HttpStatus.BAD_REQUEST;
		}

	}

}
