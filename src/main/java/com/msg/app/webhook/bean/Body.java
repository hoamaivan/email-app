package com.msg.app.webhook.bean;

import java.util.ArrayList;
import java.util.List;

public class Body {

	List<Changes> changes = new ArrayList<>();

	public List<Changes> getChanges() {
		return changes;
	}

	public void setChanges(final List<Changes> changes) {
		this.changes = changes;
	}

}