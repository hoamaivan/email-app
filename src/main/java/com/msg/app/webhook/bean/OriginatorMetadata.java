package com.msg.app.webhook.bean;

public class OriginatorMetadata {

	private String id;
	private String role;

	public String getId() {
		return id;
	}

	public String getRole() {
		return role;
	}

	public void setId(final String id) {
		this.id = id;
	}

	public void setRole(final String role) {
		this.role = role;
	}
}