package com.msg.app.webhook.bean;

public class Changes {

	private int sequence;
	private String originatorId;
	OriginatorMetadata originatorMetadata;
	private long serverTimestamp;
	Event event;
	private String dialogId;

	public int getSequence() {
		return sequence;
	}

	public void setSequence(final int sequence) {
		this.sequence = sequence;
	}

	public String getOriginatorId() {
		return originatorId;
	}

	public void setOriginatorId(final String originatorId) {
		this.originatorId = originatorId;
	}

	public OriginatorMetadata getOriginatorMetadata() {
		return originatorMetadata;
	}

	public void setOriginatorMetadata(final OriginatorMetadata originatorMetadata) {
		this.originatorMetadata = originatorMetadata;
	}

	public long getServerTimestamp() {
		return serverTimestamp;
	}

	public void setServerTimestamp(final long serverTimestamp) {
		this.serverTimestamp = serverTimestamp;
	}

	public Event getEvent() {
		return event;
	}

	public void setEvent(final Event event) {
		this.event = event;
	}

	public String getDialogId() {
		return dialogId;
	}

	public void setDialogId(final String dialogId) {
		this.dialogId = dialogId;
	}

}