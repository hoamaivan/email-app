package com.msg.app.webhook.bean;

public class ResponseContent {

	private String kind;
	Body body;
	private String type;

	public String getKind() {
		return kind;
	}

	public void setKind(final String kind) {
		this.kind = kind;
	}

	public Body getBody() {
		return body;
	}

	public void setBody(final Body body) {
		this.body = body;
	}

	public String getType() {
		return type;
	}

	public void setType(final String type) {
		this.type = type;
	}

}