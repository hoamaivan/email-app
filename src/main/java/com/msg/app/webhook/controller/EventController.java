package com.msg.app.webhook.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.msg.app.webhook.bean.ResponseContent;
import com.msg.app.webhook.service.EventService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/v1/webhooks")
public class EventController {

	@Autowired
	private EventService eventService;

	@ApiOperation("This api recieve message from webhook with type envent is ")
	@ApiResponses(value = { @ApiResponse(code = 200, response = ResponseEntity.class, message = "OK"),
			@ApiResponse(code = 500, message = "Internal Server Error"), @ApiResponse(code = 400, message = "") })
	@PostMapping("/content-event")
	public ResponseEntity<HttpStatus> receiveMessage(
			@RequestBody(required = true) final ResponseContent responseContent) {
		return new ResponseEntity<>(eventService.sendMessageToQueue(responseContent));
	}
}
