package com.msg.app.account.scheduler;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.msg.app.account.bean.BrandAccount;
import com.msg.app.account.service.BrandAccountService;
import com.msg.app.queue.QueueSender;

public class JobLoadAccountExecution implements Job {

	private static final Logger LOGGER = LoggerFactory.getLogger(JobLoadAccountExecution.class);

	@Autowired
	private BrandAccountService brandAccountService;
	@Autowired
	private QueueSender queueSender;

	@Override
	public void execute(final JobExecutionContext context) throws JobExecutionException {
		if (InternalQueue.BRAND_ACCOUNT_QUEUE.isEmpty()) {
			loadBrandAccount();
		}

		final BrandAccount account = InternalQueue.BRAND_ACCOUNT_QUEUE.poll();
		if (account != null) {
			// Pick one account from internal queue and send to RabbitMQ
			queueSender.send(account);
		} else {
			LOGGER.info("There's NO brand account from DB");
		}
	}

	private void loadBrandAccount() {
		// Empty account internal queue -> Loading from DB
		brandAccountService.findAllBrandAccount().stream().forEach(InternalQueue.BRAND_ACCOUNT_QUEUE::add);
	}
}
