package com.msg.app.account.scheduler;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;
import com.msg.app.account.bean.BrandAccount;

public class InternalQueue {

	private InternalQueue() {
	}

	static final Queue<BrandAccount> BRAND_ACCOUNT_QUEUE = new LinkedList<>();

	private static final Set<String> BRAND_IN_PROCESSING = new HashSet<>();

	public static boolean addBrandAccountInProcess(final String brandMail) {
		return BRAND_IN_PROCESSING.add(brandMail);
	}

	public static boolean containsBrandAccountInProcess(final String brandMail) {
		return BRAND_IN_PROCESSING.contains(brandMail);
	}

	public static boolean removeBrandAccountInProcess(final String brandMail) {
		return BRAND_IN_PROCESSING.remove(brandMail);
	}

}