package com.msg.app.account.scheduler;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;

@Configuration
public class JobLoadAccountConfig {

	@Value("${email-app.account.scheduler.job-detail.start-delay}")
	private Long startDelay;

	@Value("${email-app.account.scheduler.job-detail.cron-expression}")
	private String cronExpression;

	@Value("${email-app.account.scheduler.job-detail.description}")
	private String description;

	@Value("${email-app.account.scheduler.job-detail.job-name}")
	private String jobName;

	@Bean(name = "cronPushAccountTriggerBean")
	public CronTriggerFactoryBean cronPushAccountTriggerBean() {
		final CronTriggerFactoryBean stFactory = new CronTriggerFactoryBean();
		stFactory.setJobDetail(scheduleJobDetails().getObject());
		stFactory.setStartDelay(startDelay);
		stFactory.setCronExpression(cronExpression);
		return stFactory;
	}

	@Bean(name = "scheduleJobDetails")
	public JobDetailFactoryBean scheduleJobDetails() {
		final JobDetailFactoryBean jobDetailFactoryBean = new JobDetailFactoryBean();
		jobDetailFactoryBean.setJobClass(JobLoadAccountExecution.class);
		jobDetailFactoryBean.setDescription(description);
		jobDetailFactoryBean.setDurability(true);
		jobDetailFactoryBean.setName(jobName);

		return jobDetailFactoryBean;
	}

}