package com.msg.app.account.bean;

import java.io.Serializable;
import javax.validation.constraints.NotBlank;

public class HostInfo implements Serializable {

	private static final long serialVersionUID = -7298305791624778958L;

	@NotBlank
	private String protocol;
	@NotBlank
	private String host;
	private int port;

	private boolean requireSSL;

	public HostInfo() {
	}

	public HostInfo(final String protocol, final String host, final int port, final boolean requireSSL) {
		this.protocol = protocol;
		this.host = host;
		this.port = port;
		this.requireSSL = requireSSL;
	}

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(final String protocol) {
		this.protocol = protocol;
	}

	public String getHost() {
		return host;
	}

	public void setHost(final String host) {
		this.host = host;
	}

	public int getPort() {
		return port;
	}

	public void setPort(final int port) {
		this.port = port;
	}

	public boolean isRequireSSL() {
		return requireSSL;
	}

	public void setRequireSSL(final boolean requireSSL) {
		this.requireSSL = requireSSL;
	}

}
