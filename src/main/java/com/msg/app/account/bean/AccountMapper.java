package com.msg.app.account.bean;

import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.msg.app.common.util.PasswordEncryption;

@Component
public class AccountMapper {

	@Autowired
	private HostInfoMapper hostInfoMapper;

	@Autowired
	private TemplateMapper templateMapper;

	@Autowired
	private PasswordEncryption passwordEncryption;

	public BrandAccountEntity build(final BrandAccount dto) {
		return new BrandAccountEntity(dto.getBrandEmail(), dto.getBrandName(),
				passwordEncryption.encrypt(dto.getSmtpPassword()), new Date(), hostInfoMapper.map(dto.getIncoming()),
				hostInfoMapper.map(dto.getOutgoing()), new TemplateEntity("", "", "", ""));
	}

	public BrandAccount map(final BrandAccountEntity entity) {
		return new BrandAccount(entity.getBrandEmail(), entity.getBrandName(),
				passwordEncryption.decrypt(entity.getSmtpPassword()), hostInfoMapper.map(entity.getIncoming()),
				hostInfoMapper.map(entity.getOutgoing()), entity.getDateCreated(),
				templateMapper.map(entity.getTemplate()));
	}
}
