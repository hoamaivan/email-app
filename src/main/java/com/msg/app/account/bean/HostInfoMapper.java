package com.msg.app.account.bean;

import org.springframework.stereotype.Component;

@Component
public class HostInfoMapper {

	public HostInfo map(final HostInfoEntity entity) {
		return new HostInfo(entity.getProtocol(), entity.getHost(), entity.getPort(), entity.isRequireSSL());
	}

	public HostInfoEntity map(final HostInfo dto) {
		return new HostInfoEntity(dto.getProtocol(), dto.getHost(), dto.getPort(), dto.isRequireSSL());
	}
}
