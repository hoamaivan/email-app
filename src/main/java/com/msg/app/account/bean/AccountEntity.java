package com.msg.app.account.bean;

import java.util.Date;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

@Table(value = "brand_account")
public class AccountEntity {

	@PrimaryKey(value = "brand_email")
	private String brandEmail;

	@Column(value = "brand_name")
	private String brandName;

	@Column(value = "smtp_password")
	private String smtpPassword;

	@Column(value = "date_created")
	private Date dateCreated;

	@Column(value = "incoming")
	private HostInfoEntity incoming;

	@Column(value = "outgoing")
	private HostInfoEntity outgoing;

	@Column(value = "template")
	private TemplateEntity template;

	public BrandAccountEntity() {
    }

	public BrandAccountEntity(final String brandEmail, final String brandName, final String smtpPassword,
	    final Date dateCreated, final HostInfoEntity incoming, final HostInfoEntity outgoing,
	    final TemplateEntity template) {
	this.brandEmail = brandEmail;
	this.brandName = brandName;
	this.smtpPassword = smtpPassword;
	this.dateCreated = dateCreated;
	this.incoming = incoming;
	this.outgoing = outgoing;
	this.template = template;
    }

	public String getBrandEmail() {
		return brandEmail;
	}

	public void setBrandEmail(final String brandEmail) {
		this.brandEmail = brandEmail;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(final String brandName) {
		this.brandName = brandName;
	}

	public String getSmtpPassword() {
		return smtpPassword;
	}

	public void setSmtpPassword(final String smtpPassword) {
		this.smtpPassword = smtpPassword;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(final Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public HostInfoEntity getIncoming() {
		return incoming;
	}

	public void setIncoming(final HostInfoEntity incoming) {
		this.incoming = incoming;
	}

	public HostInfoEntity getOutgoing() {
		return outgoing;
	}

	public void setOutgoing(final HostInfoEntity outgoing) {
		this.outgoing = outgoing;
	}

	public TemplateEntity getTemplate() {
		return template;
	}

	public void setTemplate(final TemplateEntity template) {
		this.template = template;
	}

}
