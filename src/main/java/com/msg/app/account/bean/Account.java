package com.msg.app.account.bean;

import java.io.Serializable;
import java.util.Date;
import javax.validation.constraints.NotBlank;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class Account implements Serializable {

	private static final long serialVersionUID = 1L;

	@NotBlank
	private String brandEmail;
	@NotBlank
	private String brandName;
	@NotBlank
	private String smtpPassword;

	private HostInfo incoming;
	private HostInfo outgoing;
	private Date dateCreated;

	@JsonIgnore
	private Template template;

	public BrandAccount() {
    }

	public BrandAccount(final String brandEmail, final String brandName, final String smtpPassword,
	    final HostInfo incoming, final HostInfo outgoing, final Date dateCreated, final Template template) {
	this.brandEmail = brandEmail;
	this.brandName = brandName;
	this.smtpPassword = smtpPassword;
	this.incoming = incoming;
	this.outgoing = outgoing;
	this.dateCreated = dateCreated;
	this.template = template;
    }

	public String getBrandEmail() {
		return brandEmail;
	}

	public void setBrandEmail(final String emailBrand) {
		this.brandEmail = emailBrand;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(final String brandName) {
		this.brandName = brandName;
	}

	public String getSmtpPassword() {
		return smtpPassword;
	}

	public void setSmtpPassword(final String smtpPassword) {
		this.smtpPassword = smtpPassword;
	}

	public HostInfo getIncoming() {
		return incoming;
	}

	public void setIncoming(final HostInfo incoming) {
		this.incoming = incoming;
	}

	public HostInfo getOutgoing() {
		return outgoing;
	}

	public void setOutgoing(final HostInfo outgoing) {
		this.outgoing = outgoing;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(final Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Template getTemplate() {
		return template;
	}

	public void setTemplate(final Template template) {
		this.template = template;
	}

}
