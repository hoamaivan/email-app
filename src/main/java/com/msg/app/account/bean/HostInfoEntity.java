package com.msg.app.account.bean;

import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.UserDefinedType;

@UserDefinedType(value = "host_info")
public class HostInfoEntity {

	@Column(value = "protocol")
	private String protocol;
	@Column(value = "host")
	private String host;
	@Column(value = "port")
	private int port;
	@Column(value = "require_ssl")
	private boolean requireSSL;

	public HostInfoEntity() {
	}

	public HostInfoEntity(final String protocol, final String host, final int port, final boolean requireSSL) {
		this.protocol = protocol;
		this.host = host;
		this.port = port;
		this.requireSSL = requireSSL;
	}

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(final String protocol) {
		this.protocol = protocol;
	}

	public String getHost() {
		return host;
	}

	public void setHost(final String host) {
		this.host = host;
	}

	public int getPort() {
		return port;
	}

	public void setPort(final int port) {
		this.port = port;
	}

	public boolean isRequireSSL() {
		return requireSSL;
	}

	public void setRequireSSL(final boolean requireSSL) {
		this.requireSSL = requireSSL;
	}

}
