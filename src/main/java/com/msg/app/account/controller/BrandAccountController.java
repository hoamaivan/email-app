package com.msg.app.account.controller;

import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.msg.app.account.bean.BrandAccount;
import com.msg.app.account.bean.Template;
import com.msg.app.account.service.BrandAccountService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/v1/brand-account")
public class BrandAccountController {

	@Autowired
	BrandAccountService brandAccountService;

	@ApiOperation("This api use for create new configuration brand account")
	@ApiResponses(value = { @ApiResponse(code = 201, response = ResponseEntity.class, message = "Created"),
			@ApiResponse(code = 500, message = "Encrypt password fail"), @ApiResponse(code = 400, message = "") })
	@ResponseStatus(code = HttpStatus.CREATED)
	@PostMapping
	public void createBrandAccount(@Validated @RequestBody final BrandAccount brandAccount) {
		brandAccountService.save(brandAccount);
	}

	@ApiOperation("This api use for add header, footer for template response mail to customer")
	@ApiResponses(value = { @ApiResponse(code = 200, response = Template.class, message = ""),
			@ApiResponse(code = 400, message = "Not found brand email"),
			@ApiResponse(code = 400, message = "File size is too large"),
			@ApiResponse(code = 400, message = "Not support content type") })
	@PostMapping("/{brandEmail}/template")
	public Template createTemplate(@PathVariable final String brandEmail,
			@RequestParam(required = false, name = "header") final MultipartFile headerFile,
			@RequestParam(required = false, name = "messaging_channel") final MultipartFile channelFile,
			@RequestParam(required = false, name = "footer") final MultipartFile footerFile,
			@RequestParam(required = false, name = "body_style") final MultipartFile bodyStyleFile) throws IOException {
		return brandAccountService.createTemplate(brandEmail, headerFile, channelFile, footerFile, bodyStyleFile);
	}
}
