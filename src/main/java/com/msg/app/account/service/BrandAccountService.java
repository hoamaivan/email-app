package com.msg.app.account.service;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import org.springframework.web.multipart.MultipartFile;
import com.msg.app.account.bean.BrandAccount;
import com.msg.app.account.bean.Template;

public interface BrandAccountService {

	public List<BrandAccount> findAllBrandAccount();

	public Optional<BrandAccount> findByBrandEmail(String brandEmail);

	public void save(BrandAccount brandAccount);

	public Template createTemplate(final String brandEmail, final MultipartFile headerFile,
			final MultipartFile channelFile, final MultipartFile footerFile, final MultipartFile bodyStyleFile)
			throws IOException;

}
