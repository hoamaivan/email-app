package com.msg.app.account.service;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import com.msg.app.account.bean.BrandAccount;
import com.msg.app.account.bean.BrandAccountEntity;
import com.msg.app.account.bean.AccountMapper;
import com.msg.app.account.bean.Template;
import com.msg.app.account.bean.TemplateMapper;
import com.msg.app.account.dao.BrandAccountRepository;
import com.msg.app.common.exception.FileTypeException;
import com.msg.app.common.exception.InvalidParameterException;
import com.msg.app.config.TemplateConfig;

@Service
public class BrandAccountServiceImpl implements BrandAccountService {

	@Autowired
	private BrandAccountRepository brandAccountRepository;
	@Autowired
	private AccountMapper brandAccountMapper;
	@Autowired
	private TemplateConfig templateConfig;
	@Autowired
	private TemplateMapper templateMapper;

	@Override
	public List<BrandAccount> findAllBrandAccount() {
		return brandAccountRepository.findAll().stream().map(brandAccountMapper::map).collect(Collectors.toList());
	}

	@Override
	public void save(final BrandAccount brandAccount) {
		final BrandAccountEntity entity = brandAccountMapper.build(brandAccount);
		brandAccountRepository.save(entity);
	}

	@Override
	public Optional<BrandAccount> findByBrandEmail(final String brandEmail) {
		final Optional<BrandAccountEntity> account = brandAccountRepository.findById(brandEmail);
		if (account.isPresent()) {
			return Optional.of(brandAccountMapper.map(account.get()));
		}
		return Optional.empty();
	}

}
