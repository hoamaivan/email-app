package com.msg.app.account.dao;

import org.springframework.data.cassandra.repository.CassandraRepository;
import com.msg.app.account.bean.BrandAccountEntity;

public interface BrandAccountRepository extends CassandraRepository<BrandAccountEntity, String> {
}
