package com.msg.app.common.util;

import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;
import com.msg.app.common.exception.PasswordDecryptException;

@RunWith(MockitoJUnitRunner.class)
public class PasswordEncryptionTest {

	@InjectMocks
	private PasswordEncryption passwordEncryption;

	@Test
	public void testDecryptPasswordSuccessfully() {
		ReflectionTestUtils.setField(passwordEncryption, "salt", "123aaa");
		ReflectionTestUtils.setField(passwordEncryption, "key", "123aaa");
		final String result = passwordEncryption.encrypt("123456");
		assertThat(result).isEqualTo("v/Gi2gDhe3HaX1cm4cXIUA==");
	}

	@Test
	public void testEcryptPasswordSuccessfully() {
		ReflectionTestUtils.setField(passwordEncryption, "salt", "123aaa");
		ReflectionTestUtils.setField(passwordEncryption, "key", "123aaa");
		final String result = passwordEncryption.decrypt("v/Gi2gDhe3HaX1cm4cXIUA==");
		assertThat(result).isEqualTo("123456");
	}

	@Test(expected = PasswordDecryptException.class)
	public void testEcryptPasswordFail() {
		ReflectionTestUtils.setField(passwordEncryption, "salt", "123aaa");
		ReflectionTestUtils.setField(passwordEncryption, "key", "123aaabbb");
		final String result = passwordEncryption.decrypt("v/Gi2gDhe3HaX1cm4cXIUA==");
		assertThat(result).isEqualTo("123456");
		;
	}
}
