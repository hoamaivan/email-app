package com.msg.app.account.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import com.msg.app.account.bean.BrandAccount;
import com.msg.app.account.bean.BrandAccountEntity;
import com.msg.app.account.bean.AccountMapper;
import com.msg.app.account.bean.HostInfo;
import com.msg.app.account.bean.HostInfoEntity;
import com.msg.app.account.dao.BrandAccountRepository;
import com.msg.app.account.service.BrandAccountService;
import com.msg.app.account.service.BrandAccountServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class BrandAccountServiceImplTest {

	@Mock
	private AccountMapper brandAccountMapper;

	@Mock
	private BrandAccountRepository brandAccountRepository;

	@InjectMocks
	private final BrandAccountService brandAccountService = new BrandAccountServiceImpl();

	private BrandAccount createBrandAccount() {
		final HostInfo incoming = new HostInfo("POP3", "pop.gmail.com", 995, true);
		final HostInfo outgoing = new HostInfo("SMTP", "smtp.gmail.com", 25, false);
		return new BrandAccount("support@mail.com", "emailhuman", "1234@A", incoming, outgoing, new Date(), null);
	}

	private BrandAccountEntity createBrandAccountEntity() throws Exception {
		final HostInfoEntity incoming = new HostInfoEntity("POP3", "pop.gmail.com", 995, true);
		final HostInfoEntity outgoing = new HostInfoEntity("SMTP", "smtp.gmail.com", 25, false);

		final Date dateCreated = new SimpleDateFormat("yyyy/MM/dd").parse("2019/04/09");
		return new BrandAccountEntity("support@mail.com", "emailhuman", "1234@A", dateCreated, incoming, outgoing,
				null);
	}

	@Test
	public void testCreateAccount() throws Exception {
		when(brandAccountMapper.build(any(BrandAccount.class))).thenReturn(createBrandAccountEntity());
		brandAccountService.save(createBrandAccount());

		verify(brandAccountRepository, times(1)).save(any(BrandAccountEntity.class));
	}

	@Test
	public void testfindAllBrandAccount() throws Exception {
		final BrandAccountEntity account = createBrandAccountEntity();
		when(brandAccountRepository.findAll()).thenReturn(Arrays.asList(account));
		when(brandAccountMapper.map(any(BrandAccountEntity.class))).thenReturn(createBrandAccount());

		final List<BrandAccount> result = brandAccountService.findAllBrandAccount();

		assertThat(result.get(0).getBrandName()).isEqualTo(account.getBrandName());
		assertThat(result.get(0).getBrandEmail()).isEqualTo(account.getBrandEmail());
		assertThat(result.get(0).getSmtpPassword()).isEqualTo(account.getSmtpPassword());

		assertThat(result.get(0).getIncoming().getHost()).isEqualTo(account.getIncoming().getHost());
		assertThat(result.get(0).getIncoming().getPort()).isEqualTo(account.getIncoming().getPort());
		assertThat(result.get(0).getIncoming().getProtocol()).isEqualTo(account.getIncoming().getProtocol());
		assertThat(result.get(0).getIncoming().isRequireSSL()).isEqualTo(account.getIncoming().isRequireSSL());

		assertThat(result.get(0).getIncoming().getHost()).isEqualTo(account.getIncoming().getHost());
		assertThat(result.get(0).getIncoming().getPort()).isEqualTo(account.getIncoming().getPort());
		assertThat(result.get(0).getIncoming().getProtocol()).isEqualTo(account.getIncoming().getProtocol());
		assertThat(result.get(0).getIncoming().isRequireSSL()).isEqualTo(account.getIncoming().isRequireSSL());

	}

	@Test
	public void testFindByBrandEmailWhenNotExist() throws Exception {
		when(brandAccountRepository.findById(any(String.class))).thenReturn(Optional.empty());

		final Optional<BrandAccount> result = brandAccountService.findByBrandEmail("support@mail.com");
		assertFalse(result.isPresent());
	}

	@Test
	public void testFindByBrandEmailWhenExist() throws Exception {
		when(brandAccountRepository.findById(any(String.class))).thenReturn(Optional.of(createBrandAccountEntity()));

		final BrandAccount account = createBrandAccount();
		when(brandAccountMapper.map(any(BrandAccountEntity.class))).thenReturn(account);

		final Optional<BrandAccount> result = brandAccountService.findByBrandEmail("support@mail.com");
		assertTrue(result.isPresent());

		assertThat(result.get().getBrandName()).isEqualTo(account.getBrandName());
		assertThat(result.get().getBrandEmail()).isEqualTo(account.getBrandEmail());
		assertThat(result.get().getSmtpPassword()).isEqualTo(account.getSmtpPassword());

		assertThat(result.get().getIncoming().getHost()).isEqualTo(account.getIncoming().getHost());
		assertThat(result.get().getIncoming().getPort()).isEqualTo(account.getIncoming().getPort());
		assertThat(result.get().getIncoming().getProtocol()).isEqualTo(account.getIncoming().getProtocol());
		assertThat(result.get().getIncoming().isRequireSSL()).isEqualTo(account.getIncoming().isRequireSSL());

		assertThat(result.get().getIncoming().getHost()).isEqualTo(account.getIncoming().getHost());
		assertThat(result.get().getIncoming().getPort()).isEqualTo(account.getIncoming().getPort());
		assertThat(result.get().getIncoming().getProtocol()).isEqualTo(account.getIncoming().getProtocol());
		assertThat(result.get().getIncoming().isRequireSSL()).isEqualTo(account.getIncoming().isRequireSSL());
	}

}
