package com.msg.app.account.scheduler;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.msg.app.account.bean.BrandAccount;
import com.msg.app.account.service.BrandAccountService;
import com.msg.app.queue.QueueSender;

@RunWith(MockitoJUnitRunner.class)
public class JobLoadAccountExecutionTest {

	@Mock
	private BrandAccountService brandAccountService;

	@Mock
	private QueueSender queueSender;

	@Mock
	private JobExecutionContext context;

	@InjectMocks
	private JobLoadAccountExecution jobLoadAccountExecution;

	private List<BrandAccount> createAccountList() {
		final BrandAccount brandAccount = new BrandAccount("support@mail.com", "Email", "1234@A", null, null,
				new Date(), null);
		return Arrays.asList(brandAccount);
	}

	@Test
	public void testPushAccountIntoRabbitMQWhenInternalQueueEmptyAndHaveBrandAccountInDataBase()
			throws JobExecutionException {
		when(brandAccountService.findAllBrandAccount()).thenReturn(createAccountList());

		jobLoadAccountExecution.execute(context);

		verify(brandAccountService, times(1)).findAllBrandAccount();
		verify(queueSender, times(1)).send(any(BrandAccount.class));
	}

	@Test
	public void testPushAccountIntoRabbitMQWhenNotHaveBrandAccountInDataBase() throws Exception {
		when(brandAccountService.findAllBrandAccount()).thenReturn(new ArrayList<>());

		jobLoadAccountExecution.execute(context);

		verify(brandAccountService, times(1)).findAllBrandAccount();
		verify(queueSender, times(0)).send(any());
	}

}
