package com.msg.app.account.controller;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import java.util.Date;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.msg.app.account.bean.BrandAccount;
import com.msg.app.account.bean.HostInfo;
import com.msg.app.account.controller.BrandAccountController;
import com.msg.app.account.service.BrandAccountServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class BrandAccountControllerTest {

	private final String CREATE_BRAND_ACCOUNT_URL = "/v1/brand-account";

	private MockMvc mvc;

	@Mock
	private BrandAccountServiceImpl brandAccountServiceImpl;

	@InjectMocks
	private BrandAccountController brandAccountController;

	private BrandAccount createBrandAccount() {
		final HostInfo incoming = new HostInfo("POP3", "pop.gmail.com", 995, true);
		final HostInfo outgoing = new HostInfo("SMTP", "smtp.gmail.com", 25, false);
		return new BrandAccount("support@mail.com", "email", "123A@", incoming, outgoing, new Date(), null);
	}

	@Before
	public void setUp() throws Exception {
		mvc = MockMvcBuilders.standaloneSetup(brandAccountController).build();
	}

	@Test
	public void testCreateBrandAccount() throws Exception {

		mvc.perform(MockMvcRequestBuilders.post(CREATE_BRAND_ACCOUNT_URL).accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON).content(asJsonString(createBrandAccount())))
				.andExpect(status().isCreated());

	}

	private static String asJsonString(final Object obj) throws Exception {
		return new ObjectMapper().writeValueAsString(obj);
	}
}
