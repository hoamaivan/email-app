package com.msg.app.account.bean;

import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Test;
import com.msg.app.account.bean.HostInfo;
import com.msg.app.account.bean.HostInfoEntity;
import com.msg.app.account.bean.HostInfoMapper;

public class HostInfoMapperTest {

	private final HostInfoMapper mapper = new HostInfoMapper();

	HostInfo host = new HostInfo("POP3", "pop.gmail.com", 995, true);
	HostInfoEntity hostEntity = new HostInfoEntity("SMTP", "smtp.gmail.com", 25, false);

	@Test
	public void testMapHostInfotoHostInfoEntity() {
		final HostInfoEntity result = mapper.map(host);
		assertThat(result.getHost()).isEqualTo("pop.gmail.com");
		assertThat(result.getProtocol()).isEqualTo("POP3");
		assertThat(result.getPort()).isEqualTo(995);
		assertThat(result.isRequireSSL()).isEqualTo(true);

	}

	@Test
	public void testMapHostInfoEntitytoHostInfo() {
		final HostInfo result = mapper.map(hostEntity);
		assertThat(result.getHost()).isEqualTo("smtp.gmail.com");
		assertThat(result.getProtocol()).isEqualTo("SMTP");
		assertThat(result.getPort()).isEqualTo(25);
		assertThat(result.isRequireSSL()).isEqualTo(false);
	}
}
