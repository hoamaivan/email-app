package com.msg.app.account.bean;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import com.msg.app.account.bean.BrandAccount;
import com.msg.app.account.bean.BrandAccountEntity;
import com.msg.app.account.bean.AccountMapper;
import com.msg.app.account.bean.HostInfo;
import com.msg.app.account.bean.HostInfoEntity;
import com.msg.app.account.bean.HostInfoMapper;
import com.msg.app.account.bean.TemplateEntity;
import com.msg.app.account.bean.TemplateMapper;
import com.msg.app.common.util.PasswordEncryption;

@RunWith(MockitoJUnitRunner.class)
public class BrandAccountMapperTest {

	private static final String ENCRYPT_PASSWORD = "passwor_encryption";

	@Spy
	private HostInfoMapper mapper;

	@Spy
	private TemplateMapper templateMapper;

	@Mock
	private PasswordEncryption passwordEncryption;

	@InjectMocks
	private final AccountMapper brandAccountMapper = new AccountMapper();

	@Before
	public void setUp() throws Exception {
		when(passwordEncryption.decrypt(any(String.class))).thenReturn("1234@A");
		when(passwordEncryption.encrypt(any(String.class))).thenReturn(ENCRYPT_PASSWORD);
	}

	private BrandAccount createBrandAccount() {
		final HostInfo incoming = new HostInfo("POP3", "pop.gmail.com", 995, true);
		final HostInfo outgoing = new HostInfo("SMTP", "smtp.gmail.com", 25, false);

		return new BrandAccount("support@mail.com", "emailhuman", "1234@A", incoming, outgoing, new Date(), null);
	}

	private BrandAccountEntity createBrandAccountEntity() throws Exception {
		final Date dateCreated = new SimpleDateFormat("yyyy/MM/dd").parse("2019/04/09");
		final HostInfoEntity incoming = new HostInfoEntity("POP3", "pop.gmail.com", 995, true);
		final HostInfoEntity outgoing = new HostInfoEntity("SMTP", "smtp.gmail.com", 25, false);
		final TemplateEntity template = new TemplateEntity("", "", "", "");
		return new BrandAccountEntity("support@mail.com", "emailhuman", "1234@A", dateCreated, incoming, outgoing,
				template);
	}

	@Test
	public void testBuildBrandAccountEntityFromBrandAccount() {
		final BrandAccount account = createBrandAccount();
		final BrandAccountEntity result = brandAccountMapper.build(account);

		assertThat(result.getBrandName()).isEqualTo(account.getBrandName());
		assertThat(result.getBrandEmail()).isEqualTo(account.getBrandEmail());
		assertThat(result.getSmtpPassword()).isEqualTo(ENCRYPT_PASSWORD);

		assertThat(result.getIncoming().getHost()).isEqualTo(account.getIncoming().getHost());
		assertThat(result.getIncoming().getPort()).isEqualTo(account.getIncoming().getPort());
		assertThat(result.getIncoming().getProtocol()).isEqualTo(account.getIncoming().getProtocol());
		assertThat(result.getIncoming().isRequireSSL()).isEqualTo(account.getIncoming().isRequireSSL());

		assertThat(result.getOutgoing().getHost()).isEqualTo(account.getOutgoing().getHost());
		assertThat(result.getOutgoing().getPort()).isEqualTo(account.getOutgoing().getPort());
		assertThat(result.getOutgoing().getProtocol()).isEqualTo(account.getOutgoing().getProtocol());
		assertThat(result.getOutgoing().isRequireSSL()).isEqualTo(account.getOutgoing().isRequireSSL());

		assertNotNull(result.getDateCreated());
	}

	@Test
	public void testMapBrandAccountEntityToBrandAccount() throws Exception {
		final BrandAccountEntity account = createBrandAccountEntity();
		final BrandAccount result = brandAccountMapper.map(account);

		assertThat(result.getBrandName()).isEqualTo(account.getBrandName());
		assertThat(result.getBrandEmail()).isEqualTo(account.getBrandEmail());
		assertThat(result.getSmtpPassword()).isEqualTo(account.getSmtpPassword());

		assertThat(result.getIncoming().getHost()).isEqualTo(account.getIncoming().getHost());
		assertThat(result.getIncoming().getPort()).isEqualTo(account.getIncoming().getPort());
		assertThat(result.getIncoming().getProtocol()).isEqualTo(account.getIncoming().getProtocol());
		assertThat(result.getIncoming().isRequireSSL()).isEqualTo(account.getIncoming().isRequireSSL());

		assertThat(result.getOutgoing().getHost()).isEqualTo(account.getOutgoing().getHost());
		assertThat(result.getOutgoing().getPort()).isEqualTo(account.getOutgoing().getPort());
		assertThat(result.getOutgoing().getProtocol()).isEqualTo(account.getOutgoing().getProtocol());
		assertThat(result.getOutgoing().isRequireSSL()).isEqualTo(account.getOutgoing().isRequireSSL());

		assertThat(result.getTemplate().getHeader()).isEqualTo(account.getTemplate().getHeader());
		assertThat(result.getTemplate().getFooter()).isEqualTo(account.getTemplate().getFooter());
	}
}
