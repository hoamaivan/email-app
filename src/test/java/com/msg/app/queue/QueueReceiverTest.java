package com.msg.app.queue;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import javax.mail.MessagingException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import com.msg.app.account.bean.BrandAccount;
import com.msg.app.conversation.service.ConversationService;
import com.msg.app.email.pulling.service.PullingService;
import com.msg.app.webhook.bean.Body;
import com.msg.app.webhook.bean.Changes;
import com.msg.app.webhook.bean.Event;
import com.msg.app.webhook.bean.OriginatorMetadata;
import com.msg.app.webhook.bean.ResponseContent;

@RunWith(MockitoJUnitRunner.class)
public class QueueReceiverTest {

	@Mock
	private RabbitTemplate rabbitTemplate;

	@Mock
	private PullingService emailPullingService;

	@Mock
	private ConversationService conversationService;

	@InjectMocks
	private QueueReceiver queueReceiver;

	private BrandAccount createBrandAccount() {
		return new BrandAccount("support@mail.com", "emailhuman", "1234@A", null, null, new Date(), null);
	}

	private ResponseContent createResponseContent() {
		final Event event = new Event();
		event.setContentType("text/plain");
		event.setMessage("Hello from the Agent");
		event.setType("ContentEvent");

		final OriginatorMetadata originatorMetadata = new OriginatorMetadata();
		originatorMetadata.setId("29c3019d-fe0b-5200-afe8-737abfeae4b4");
		originatorMetadata.setRole("ASSIGNED_AGENT");

		final Changes change = new Changes();
		change.setEvent(event);
		change.setOriginatorMetadata(originatorMetadata);
		change.setDialogId("5491244f-e462-4c63-bf4e-faf4b2d3e7b5");
		change.setSequence(2);
		change.setOriginatorId("29c3019d-fe0b-5200-afe8-737abfeae4b4");
		change.setServerTimestamp(1528371199686L);

		final Body body = new Body();
		body.setChanges(Arrays.asList(change));

		final ResponseContent response = new ResponseContent();
		response.setBody(body);
		response.setKind("notification");
		response.setType("ms.MessagingEventNotification");

		return response;
	}

	@Test
	public void testHandleMessageWhenRecieveNullMessage() throws MessagingException, IOException {
		queueReceiver.handleMessage((BrandAccount) null);
		verify(emailPullingService, times(0)).pullEmailFromSMTP(any(BrandAccount.class));
	}

	@Test
	public void testHandleMessageWhenPullingMailSuccessfully() throws MessagingException, IOException {
		doNothing().when(emailPullingService).pullEmailFromSMTP(any(BrandAccount.class));
		queueReceiver.handleMessage(createBrandAccount());
		verify(emailPullingService, times(1)).pullEmailFromSMTP(any(BrandAccount.class));
	}

	@Test
	public void testHandleMessageWhenPullingMailThrowException() throws MessagingException, IOException {
		doThrow(new MessagingException()).when(emailPullingService).pullEmailFromSMTP(any(BrandAccount.class));
		queueReceiver.handleMessage(createBrandAccount());
		verify(emailPullingService, times(1)).pullEmailFromSMTP(any(BrandAccount.class));
	}

	@Test
	public void testRecieveMessageFromWebhookWithNull() throws Exception {
		queueReceiver.handleMessage((ResponseContent) null);
		verify(conversationService, times(0)).respondToCustomer(any(ResponseContent.class));
	}

	@Test
	public void testRecieveMessageFromWebhookWithAndSendResponseToCustomerSuccessfully() throws Exception {
		queueReceiver.handleMessage(createResponseContent());
		verify(conversationService, times(1)).respondToCustomer(any(ResponseContent.class));
	}

	@Test
	public void testRecieveMessageFromWebhookWithAndSendResponseToCustomerFail() throws Exception {
		doThrow(new MessagingException("Send mail fail.")).when(conversationService)
				.respondToCustomer(any(ResponseContent.class));
		queueReceiver.handleMessage(createResponseContent());
		verify(conversationService, times(1)).respondToCustomer(any(ResponseContent.class));
	}
}
