package com.msg.app.statistic.scheduler;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import com.msg.app.statistic.StatisticInMemory;
import com.msg.app.statistic.StatisticRepository;
import com.msg.app.statistic.bean.Statistic;
import com.msg.app.statistic.bean.StatisticEntity;

@RunWith(MockitoJUnitRunner.class)
public class JobSavingStatisticExecuteTest {

	@Mock
	private StatisticRepository statisticRepository;

	@InjectMocks
	private JobSavingStatisticExecute jobSavingStatisticExecute;

	@Captor
	private ArgumentCaptor<List<StatisticEntity>> captor;

	private Statistic createStatistic() {
		return new Statistic("supporter@mail.com", 10, 9, 8, 7, 6);
	}

	@Before
	public void setUp() throws Exception {
		StatisticInMemory.removeAllBrandStatistic();
		StatisticInMemory.addBrandStatistic(createStatistic());
	}

	@Test
	public void testSaveBrandStatistic() throws JobExecutionException {
		when(statisticRepository.saveAll(captor.capture())).thenReturn(new ArrayList<StatisticEntity>());
		jobSavingStatisticExecute.execute((JobExecutionContext) null);

		final List<StatisticEntity> result = captor.getValue();
		assertThat(result.size()).isEqualTo(1);
		assertThat(result.get(0).getTotalMailPulling()).isEqualTo(10);
		assertThat(result.get(0).getTotalMsgLESuccess()).isEqualTo(9);
		assertThat(result.get(0).getTotalMsgLEFail()).isEqualTo(8);
		assertThat(result.get(0).getTotalMsgAgent()).isEqualTo(7);
		assertThat(result.get(0).getTotalMailSmtp()).isEqualTo(6);
	}

}
