package com.msg.app.email.pulling.service;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;
import com.icegreen.greenmail.util.GreenMail;
import com.icegreen.greenmail.util.GreenMailUtil;
import com.icegreen.greenmail.util.ServerSetupTest;
import com.msg.app.account.bean.BrandAccount;
import com.msg.app.account.bean.HostInfo;
import com.msg.app.conversation.service.ConversationService;
import com.msg.app.email.bean.Mail;
import com.msg.app.email.parsing.service.ParsingService;
import com.msg.app.email.pulling.service.PullingServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class PullingServiceImplTest {

	private final String MAIL_SUBJECT = "Mail support";
	private final String SENDER_ADDRESS = "customer@mail.com";
	private final String SENDER_humanAL = "Customer";
	private final String RECIPIENT_ADDRESS = "support@mail.support.com";
	private final String RECIPIENT_humanAL = "Supporter";
	private final String CONTENT = "Mail support";
	private final String DATE_SEND = "Mon, 07 Apr 2019 09:33:01 +0700 (ICT)";
	private final String PASSWORD = "123546";

	@Mock
	ParsingService parsingService;

	@Mock
	private ConversationService conversationService;

	@InjectMocks
	PullingServiceImpl pullingServiceImpl = new PullingServiceImpl();

	public GreenMail greenMail = new GreenMail(ServerSetupTest.SMTP_POP3_IMAP);

	private final SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");

	@Before
	public void setUp() throws Exception {
		// Create user in fake smtp
		greenMail.setUser(RECIPIENT_ADDRESS, RECIPIENT_ADDRESS, PASSWORD);
		greenMail.start();
	}

	@After
	public void tearDown() throws Exception {
		greenMail.stop();
	}

	private BrandAccount createBrandAccountWithPOP3() throws Exception {
		final HostInfo incoming = new HostInfo(greenMail.getPop3().getProtocol(), "127.0.0.1",
				greenMail.getPop3().getPort(), false);
		final HostInfo outgoing = new HostInfo(greenMail.getSmtp().getProtocol(), "127.0.0.1",
				greenMail.getSmtp().getPort(), false);

		return new BrandAccount(RECIPIENT_ADDRESS, "emailhuman", PASSWORD, incoming, outgoing,
				format.parse("2019/01/01"), null);
	}

	private BrandAccount createBrandAccountWithIMAP() throws Exception {
		final HostInfo incoming = new HostInfo(greenMail.getImap().getProtocol(), "127.0.0.1",
				greenMail.getImap().getPort(), false);
		final HostInfo outgoing = new HostInfo(greenMail.getSmtp().getProtocol(), "127.0.0.1",
				greenMail.getSmtp().getPort(), false);

		return new BrandAccount(RECIPIENT_ADDRESS, "emailhuman", PASSWORD, incoming, outgoing,
				format.parse("2019/01/01"), null);
	}

	private void sendEmail() throws Exception {
		final Session smtpSession = greenMail.getSmtp().createSession();

		final MimeMessage message = new MimeMessage(smtpSession);

		message.setFrom(new InternetAddress(SENDER_ADDRESS, SENDER_humanAL));

		final Address[] sender = new Address[1];
		sender[0] = new InternetAddress(RECIPIENT_ADDRESS, RECIPIENT_humanAL);

		message.setRecipients(Message.RecipientType.TO, sender);
		message.setSubject(MAIL_SUBJECT);

		final BodyPart messageTextPart = new MimeBodyPart();
		messageTextPart.setText(CONTENT);

		final Multipart multipart = new MimeMultipart();
		multipart.addBodyPart(messageTextPart);
		message.setContent(multipart);

		message.setHeader("Date", DATE_SEND);
		GreenMailUtil.sendMimeMessage(message);
	}

	private void sendEmailExpiredDate() throws Exception {
		final Session smtpSession = greenMail.getSmtp().createSession();

		final MimeMessage message = new MimeMessage(smtpSession);

		message.setFrom(new InternetAddress(SENDER_ADDRESS, SENDER_humanAL));

		final Address[] sender = new Address[1];
		sender[0] = new InternetAddress(RECIPIENT_ADDRESS, RECIPIENT_humanAL);

		message.setRecipients(Message.RecipientType.TO, sender);
		message.setSubject(MAIL_SUBJECT);

		final BodyPart messageTextPart = new MimeBodyPart();
		messageTextPart.setText(CONTENT);

		final Multipart multipart = new MimeMultipart();
		multipart.addBodyPart(messageTextPart);
		message.setContent(multipart);

		message.setHeader("Date", "Mon, 07 Apr 2018 09:33:01 +0700 (ICT)");
		GreenMailUtil.sendMimeMessage(message);
	}

	private List<Mail> createMail() {
		final Mail mail = new Mail("Message-Id", new com.msg.app.email.bean.Address(SENDER_ADDRESS, SENDER_humanAL),
				new com.msg.app.email.bean.Address(RECIPIENT_ADDRESS, RECIPIENT_humanAL), new Date(), MAIL_SUBJECT,
				CONTENT);
		return Arrays.asList(mail);
	}

	@Test
	public void testPullingMailWithPOP3WhenContainMessage() throws Exception {
		when(parsingService.parseEmailContent(any(Message[].class), any(String.class))).thenReturn(createMail());
		ReflectionTestUtils.setField(pullingServiceImpl, "batchNumber", 1);
		sendEmail();
		sendEmail();
		pullingServiceImpl.pullEmailFromSMTP(createBrandAccountWithPOP3());
		verify(parsingService, times(1)).parseEmailContent(any(Message[].class), any(String.class));
		verify(conversationService, times(1)).createConversationAndSendMsg(ArgumentMatchers.<List<Mail>>any());
	}

	@Test
	public void testPullingMailWithPOP3WhenNotContainMessageValid() throws Exception {
		sendEmailExpiredDate();
		ReflectionTestUtils.setField(pullingServiceImpl, "batchNumber", 2);
		pullingServiceImpl.pullEmailFromSMTP(createBrandAccountWithPOP3());
		verify(parsingService, times(0)).parseEmailContent(any(Message[].class), any(String.class));
		verify(conversationService, times(0)).createConversationAndSendMsg(ArgumentMatchers.<List<Mail>>any());
	}

	@Test
	public void testPullingMailWithPOP3WhenNotContainMessage() throws Exception {
		ReflectionTestUtils.setField(pullingServiceImpl, "batchNumber", 2);
		pullingServiceImpl.pullEmailFromSMTP(createBrandAccountWithPOP3());
		verify(parsingService, times(0)).parseEmailContent(any(Message[].class), any(String.class));
		verify(conversationService, times(0)).createConversationAndSendMsg(ArgumentMatchers.<List<Mail>>any());
	}

	@Test
	public void testPullingMailWithIMAP3WhenContainMessage() throws Exception {
		when(parsingService.parseEmailContent(any(Message[].class), any(String.class))).thenReturn(createMail());
		ReflectionTestUtils.setField(pullingServiceImpl, "batchNumber", 1);
		sendEmail();
		pullingServiceImpl.pullEmailFromSMTP(createBrandAccountWithIMAP());
		verify(parsingService, times(1)).parseEmailContent(any(Message[].class), any(String.class));
		verify(conversationService, times(1)).createConversationAndSendMsg(ArgumentMatchers.<List<Mail>>any());
	}

	@Test
	public void testPullingMailWithIMAPWhenNotContainMessage() throws Exception {
		ReflectionTestUtils.setField(pullingServiceImpl, "batchNumber", 1);
		pullingServiceImpl.pullEmailFromSMTP(createBrandAccountWithIMAP());
		verify(parsingService, times(0)).parseEmailContent(any(Message[].class), any(String.class));
		verify(conversationService, times(0)).createConversationAndSendMsg(ArgumentMatchers.<List<Mail>>any());
	}

}
