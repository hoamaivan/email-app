package com.msg.app.email.parsing.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import com.icegreen.greenmail.junit.GreenMailRule;
import com.icegreen.greenmail.util.ServerSetupTest;
import com.msg.app.email.bean.Mail;
import com.msg.app.email.parsing.service.ParsingServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class ParsingServiceImplTest {

	private final String MAIL_SUBJECT = "Mail support";

	private final String SENDER_ADDRESS = "customer@mail.com";
	private final String SENDER_humanAL = "Customer";

	private final String RECIPIENT_ADDRESS = "support@mail.support.com";
	private final String RECIPIENT_humanAL = "Supporter";

	private final String CONTENT = "Mail support";

	private final String DATE_SEND = "Mon, 08 Apr 2019 09:33:01 +0700 (ICT)";

	@InjectMocks
	private ParsingServiceImpl parsingServiceImpl;

	@Rule
	public GreenMailRule greenMail = new GreenMailRule(ServerSetupTest.SMTP_POP3);

	@Before
	public void setUp() throws Exception {
		// Create user in fake smtp
		greenMail.setUser(RECIPIENT_ADDRESS, RECIPIENT_ADDRESS, "123456");
		sendEmail();
	}

	private void sendEmail() throws Exception {
		final Session smtpSession = greenMail.getSmtp().createSession();

		final Message message = new MimeMessage(smtpSession);

		message.setFrom(new InternetAddress(SENDER_ADDRESS, SENDER_humanAL));

		final Address[] sender = new Address[1];
		sender[0] = new InternetAddress(RECIPIENT_ADDRESS, RECIPIENT_humanAL);

		message.setRecipients(Message.RecipientType.TO, sender);
		message.setSubject(MAIL_SUBJECT);

		final BodyPart messageTextPart = new MimeBodyPart();
		messageTextPart.setText(CONTENT);

		final Multipart multipart = new MimeMultipart();
		multipart.addBodyPart(messageTextPart);
		message.setContent(multipart);

		message.setHeader("Date", DATE_SEND);
		Transport.send(message);
	}

	private Message[] getMessage() throws Exception {
		final Session imapSession = greenMail.getPop3().createSession();
		final Store store = imapSession.getStore("pop3");
		store.connect(RECIPIENT_ADDRESS, "123456");
		final Folder inbox = store.getFolder("INBOX");
		inbox.open(Folder.READ_WRITE);
		return inbox.getMessages();
	}

	@Test
	public void testParsingEmail() throws Exception {
		final Date sendDate = (new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss Z")).parse(DATE_SEND);
		final Message[] message = getMessage();
		final List<Mail> mailContentAfterParse = this.parsingServiceImpl.parseEmailContent(message, RECIPIENT_ADDRESS);

		assertFalse(mailContentAfterParse.get(0).getMessageId().isEmpty());

		assertThat(mailContentAfterParse.get(0).getContent()).isEqualTo(CONTENT);
		assertThat(mailContentAfterParse.get(0).getSubject()).isEqualTo(MAIL_SUBJECT);
		assertThat(mailContentAfterParse.get(0).getSentDate().compareTo(sendDate)).isEqualTo(0); // equal

		assertThat(mailContentAfterParse.get(0).getFrom().getEmailAddress()).isEqualTo(SENDER_ADDRESS);
		assertThat(mailContentAfterParse.get(0).getFrom().getName()).isEqualTo(SENDER_humanAL);

		assertThat(mailContentAfterParse.get(0).getTo().getEmailAddress()).isEqualTo(RECIPIENT_ADDRESS);
		assertTrue(mailContentAfterParse.get(0).getTo().getName().isEmpty());

	}

}
