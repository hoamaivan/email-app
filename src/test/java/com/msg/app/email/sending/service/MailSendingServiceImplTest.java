package com.msg.app.email.sending.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;
import java.util.Date;
import java.util.Optional;
import javax.mail.Message;
import javax.mail.MessagingException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import com.icegreen.greenmail.util.GreenMail;
import com.icegreen.greenmail.util.ServerSetupTest;
import com.msg.app.account.bean.BrandAccount;
import com.msg.app.account.bean.HostInfo;
import com.msg.app.account.bean.Template;
import com.msg.app.account.service.BrandAccountService;
import com.msg.app.common.exception.InvalidParameterException;
import com.msg.app.config.SendingMailConfig;
import com.msg.app.email.bean.Address;
import com.msg.app.email.bean.Mail;
import com.msg.app.email.sending.service.MailSendingServiceImpl;
import com.msg.app.email.template.TemplateService;

@RunWith(MockitoJUnitRunner.class)
public class MailSendingServiceImplTest {

	private static String BRAND_MAIL = "support@mail.com";
	private static String PASSWORD = "123A@";

	private static String MAIL_SUBJECT = "Mail subject";
	private static String MAIL_CONTENT = "Mail content";
	private static String MAIL_HISTORY = "Mail history";

	@Mock
	private BrandAccountService brandAccountService;

	@Mock
	private SendingMailConfig sendingMailConfig;

	@Spy
	private TemplateService templateService;

	@InjectMocks
	private final MailSendingServiceImpl mailSendingServiceImpl = new MailSendingServiceImpl();

	GreenMail greenMail = new GreenMail(ServerSetupTest.SMTP_POP3);

	@Before
	public void setUp() throws Exception {
		when(sendingMailConfig.getConnectionTimeout()).thenReturn(10000);
		greenMail.start();
		greenMail.setUser(BRAND_MAIL, BRAND_MAIL, PASSWORD);
	}

	@After
	public void tearDown() throws Exception {
		greenMail.stop();
	}

	private BrandAccount createBrandAccount() {
		final HostInfo incoming = new HostInfo(greenMail.getPop3().getProtocol(), greenMail.getPop3().getBindTo(),
				greenMail.getPop3().getPort(), false);

		final HostInfo outgoing = new HostInfo(greenMail.getSmtp().getProtocol(), greenMail.getSmtp().getBindTo(),
				greenMail.getSmtp().getPort(), false);

		final Template template = new Template("", "", "", "");

		return new BrandAccount(BRAND_MAIL, "emailhuman", PASSWORD, incoming, outgoing, new Date(), template);
	}

	private Mail createMail() {
		final Mail mail = new Mail();
		final Address from = new Address(BRAND_MAIL, "Brand name");
		final Address to = new Address("customer@customer.mail.com", "Customer name");
		mail.setFrom(from);
		mail.setTo(to);
		mail.setSubject(MAIL_SUBJECT);
		mail.setContent(MAIL_CONTENT);
		return mail;
	}

	@Test(expected = InvalidParameterException.class)
	public void testSendMailWhenNotHaveBrandAccount() throws MessagingException {
		when(brandAccountService.findByBrandEmail(BRAND_MAIL)).thenReturn(Optional.empty());
		mailSendingServiceImpl.sendMail(createMail(), MAIL_HISTORY);
	}

	@Test
	public void testSendMailWhenSendMailSuccessfully() throws Exception {
		assertTrue(greenMail.getReceivedMessages().length == 0);
		when(brandAccountService.findByBrandEmail(BRAND_MAIL)).thenReturn(Optional.of(createBrandAccount()));

		mailSendingServiceImpl.sendMail(createMail(), MAIL_HISTORY);

		final Message[] mails = greenMail.getReceivedMessages();
		assertTrue(mails.length == 1);
		assertEquals(mails[0].getSubject(), MAIL_SUBJECT);
	}

}
