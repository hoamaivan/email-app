package com.msg.app.conversation.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import javax.mail.MessagingException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpHeaders;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.msg.app.common.exception.NotFoundException;
import com.msg.app.conversation.HeaderInterceptor;
import com.msg.app.conversation.bean.ConversationEntity;
import com.msg.app.conversation.bean.MessageType;
import com.msg.app.conversation.dao.ConversationRepository;
import com.msg.app.conversation.service.ConversationServiceImpl;
import com.msg.app.email.bean.Address;
import com.msg.app.email.bean.Mail;
import com.msg.app.email.sending.service.MailSendingService;
import com.msg.app.webhook.bean.Body;
import com.msg.app.webhook.bean.Changes;
import com.msg.app.webhook.bean.Event;
import com.msg.app.webhook.bean.OriginatorMetadata;
import com.msg.app.webhook.bean.ResponseContent;
import com.msg.app.integrate.le.conversation.ConversationLEService;
import com.msg.app.integrate.le.message.MessageService;

@RunWith(MockitoJUnitRunner.class)
public class ConversationServiceImplTest {

	@Mock
	private MessageService messageService;
	@Mock
	private ConversationRepository conversationRepository;
	@Mock
	private ObjectMapper objectMapper;
	@Mock
	private HeaderInterceptor headerInterceptor;
	@Mock
	private MailSendingService mailSendingService;
	@Mock
	private ConversationLEService conversationLEService;

	@Captor
	private ArgumentCaptor<ConversationEntity> captor;

	@InjectMocks
	private ConversationServiceImpl conversationServiceImpl;

	@Before
	public void setUp() throws Exception {
	}

	private ResponseContent createResponseContent() {
		final Event event = new Event();
		event.setContentType("text/plain");
		event.setMessage("Hello from the Agent");
		event.setType("ContentEvent");

		final OriginatorMetadata originatorMetadata = new OriginatorMetadata();
		originatorMetadata.setId("29c3019d-fe0b-5200-afe8-737abfeae4b4");
		originatorMetadata.setRole("ASSIGNED_AGENT");

		final Changes change = new Changes();
		change.setEvent(event);
		change.setOriginatorMetadata(originatorMetadata);
		change.setDialogId("5491244f-e462-4c63-bf4e-faf4b2d3e7b5");
		change.setSequence(2);
		change.setOriginatorId("29c3019d-fe0b-5200-afe8-737abfeae4b4");
		change.setServerTimestamp(1528371199686L);

		final Body body = new Body();
		body.setChanges(Arrays.asList(change));

		final ResponseContent response = new ResponseContent();
		response.setBody(body);
		response.setKind("notification");
		response.setType("ms.MessagingEventNotification");

		return response;
	}

	private ConversationEntity createConversationEntity() {
		final MessageType message = new MessageType();
		message.setSequence(2);
		message.setAgentId("");
		message.setContent("Content from agent");
		message.setDateCreated(new Date());
		message.setFromCustomer(false);
		message.setSubject("Content from agent");
		final List<MessageType> messages = new ArrayList<>();
		messages.add(message);

		final ConversationEntity conversation = new ConversationEntity();
		conversation.setBrandEmail("support@mail.com");
		conversation.setCustomerEmail("customer@mail.com");
		conversation.setId(UUID.fromString("29c3019d-fe0b-5200-afe8-737abfeae4b4"));
		conversation.setMessage(messages);
		conversation.setJwsExtConsumerId("ExtConsumerId");
		return conversation;
	}

	private Mail createMail() {
		final Mail mail = new Mail();
		mail.setSubject("Reply to customer");
		mail.setContent("Content for reply to customer");
		mail.setFrom(new Address("customer@mail.com", "Customer"));
		mail.setTo(new Address("support@mail.com", "Supporter"));
		return mail;
	}

	@Test(expected = NotFoundException.class)
	public void testRespondToCustomerWhenNotExistConversationId() throws MessagingException {
		when(conversationRepository.findById(any(UUID.class))).thenReturn(Optional.empty());

		conversationServiceImpl.respondToCustomer(createResponseContent());

		verify(mailSendingService, times(0)).sendMail(any(Mail.class), any(String.class));
		verify(conversationRepository, times(0)).save(any());
		verify(conversationLEService, times(0)).closeConversation(any(HttpHeaders.class), any(String.class));
	}

	@Test(expected = MessagingException.class)
	public void testRespondToCustomerWhenSendMailError() throws MessagingException {
		when(conversationRepository.findById(any(UUID.class))).thenReturn(Optional.of(createConversationEntity()));
		doThrow(new MessagingException("Send mail error")).when(mailSendingService).sendMail(any(Mail.class),
				any(String.class));
		;
		try {
			conversationServiceImpl.respondToCustomer(createResponseContent());
		} catch (final MessagingException e) {

			verify(mailSendingService, times(1)).sendMail(any(Mail.class), any(String.class));
			verify(conversationRepository, times(0)).save(any());
			verify(conversationLEService, times(0)).closeConversation(any(HttpHeaders.class), any(String.class));
			throw e;
		}
	}

	@Test
	public void testRespondToCustomerSuccessfully() throws Exception {
		when(conversationRepository.findById(any(UUID.class))).thenReturn(Optional.of(createConversationEntity()));
		when(headerInterceptor.buildHttpHeaders(any(String.class))).thenReturn(new HttpHeaders());
		doNothing().when(mailSendingService).sendMail(any(Mail.class), any(String.class));
		;
		when(conversationLEService.closeConversation(any(HttpHeaders.class), any(String.class))).thenReturn("OK");

		conversationServiceImpl.respondToCustomer(createResponseContent());

		verify(mailSendingService, times(1)).sendMail(any(Mail.class), any(String.class));
		verify(conversationRepository, times(1)).save(any());
		verify(conversationLEService, times(1)).closeConversation(any(HttpHeaders.class), any(String.class));
	}

	@Test
	public void testCreateConversationAndSendMessageSuccessfully() throws Exception {
		when(headerInterceptor.buildHttpHeaders(any(String.class))).thenReturn(new HttpHeaders());
		when(conversationLEService.createNewConversation(any(HttpHeaders.class), any(Mail.class)))
				.thenReturn("29c3019d-fe0b-5200-afe8-737abfeae4b4");
		when(messageService.sendMessage(any(HttpHeaders.class), any(String.class), any(Mail.class))).thenReturn("Ok");
		when(conversationRepository.save(captor.capture())).thenReturn(null);

		conversationServiceImpl.createConversationAndSendMsg(Arrays.asList(createMail()));

		verify(conversationRepository, times(1)).save(any());
		final ConversationEntity resultCapture = captor.getValue();

		assertThat(resultCapture.getBrandEmail()).isEqualTo("support@mail.com");
		assertThat(resultCapture.getCustomerEmail()).isEqualTo("customer@mail.com");
		assertThat(resultCapture.getId().toString()).isEqualTo("29c3019d-fe0b-5200-afe8-737abfeae4b4");
		assertThat(resultCapture.getMessage().get(0).getContent()).isEqualTo("Content for reply to customer");
	}

	@Test
	public void testCreateConversationAndSendMessageWhenFailSendMessage() throws Exception {
		when(headerInterceptor.buildHttpHeaders(any(String.class))).thenReturn(new HttpHeaders());
		when(conversationLEService.createNewConversation(any(HttpHeaders.class), any(Mail.class)))
				.thenReturn("29c3019d-fe0b-5200-afe8-737abfeae4b4");
		when(messageService.sendMessage(any(HttpHeaders.class), any(String.class), any(Mail.class)))
				.thenReturn("Bad Request");
		when(conversationRepository.save(captor.capture())).thenReturn(null);

		conversationServiceImpl.createConversationAndSendMsg(Arrays.asList(createMail()));

		verify(conversationRepository, times(1)).save(any());
		final ConversationEntity resultCapture = captor.getValue();

		assertThat(resultCapture.getMessage().size()).isEqualTo(0);
	}

	@Test
	public void testCreateConversationAndSendMessageFail() {
		when(headerInterceptor.buildHttpHeaders(any(String.class))).thenReturn(new HttpHeaders());
		when(conversationLEService.createNewConversation(any(HttpHeaders.class), any(Mail.class))).thenReturn("");

		conversationServiceImpl.createConversationAndSendMsg(Arrays.asList(createMail()));

		verify(messageService, times(0)).sendMessage(any(HttpHeaders.class), any(String.class), any(Mail.class));
		verify(conversationRepository, times(0)).save(any());
	}
}
