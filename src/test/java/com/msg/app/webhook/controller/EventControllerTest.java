package com.msg.app.webhook.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import java.util.Arrays;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.msg.app.webhook.bean.Body;
import com.msg.app.webhook.bean.Changes;
import com.msg.app.webhook.bean.Event;
import com.msg.app.webhook.bean.OriginatorMetadata;
import com.msg.app.webhook.bean.ResponseContent;
import com.msg.app.webhook.controller.EventController;
import com.msg.app.webhook.service.EventService;

@RunWith(MockitoJUnitRunner.class)
public class EventControllerTest {

	private final String RECIEVE_MESSAGE = "/v1/webhooks/content-event";

	private MockMvc mvc;

	@Mock
	private EventService eventService;

	@InjectMocks
	private EventController eventController;

	@Before
	public void setUp() throws Exception {
		mvc = MockMvcBuilders.standaloneSetup(eventController).build();
	}

	private ResponseContent createResponseContent() {
		final Event event = new Event();
		event.setContentType("text/plain");
		event.setMessage("Hello from the Agent");
		event.setType("ContentEvent");

		final OriginatorMetadata originatorMetadata = new OriginatorMetadata();
		originatorMetadata.setId("29c3019d-fe0b-5200-afe8-737abfeae4b4");
		originatorMetadata.setRole("ASSIGNED_AGENT");

		final Changes change = new Changes();
		change.setEvent(event);
		change.setOriginatorMetadata(originatorMetadata);
		change.setDialogId("5491244f-e462-4c63-bf4e-faf4b2d3e7b5");
		change.setSequence(2);
		change.setOriginatorId("29c3019d-fe0b-5200-afe8-737abfeae4b4");
		change.setServerTimestamp(1528371199686L);

		final Body body = new Body();
		body.setChanges(Arrays.asList(change));

		final ResponseContent response = new ResponseContent();
		response.setBody(body);
		response.setKind("notification");
		response.setType("ms.MessagingEventNotification");

		return response;
	}

	@Test
	public void testReceiveMessageSuccessfully() throws Exception {
		when(eventService.sendMessageToQueue(any(ResponseContent.class))).thenReturn(HttpStatus.OK);

		mvc.perform(MockMvcRequestBuilders.post(RECIEVE_MESSAGE).accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON).content(asJsonString(createResponseContent())))
				.andExpect(status().isOk());

	}

	@Test
	public void testReceiveMessageSendMessageToQueueError() throws Exception {
		when(eventService.sendMessageToQueue(any(ResponseContent.class))).thenReturn(HttpStatus.INTERNAL_SERVER_ERROR);

		mvc.perform(MockMvcRequestBuilders.post(RECIEVE_MESSAGE).accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON).content(asJsonString(createResponseContent())))
				.andExpect(status().is5xxServerError());

	}

	@Test
	public void testReceiveMessageWhenNotHaveRequestBodyThenBadRequest() throws Exception {
		mvc.perform(MockMvcRequestBuilders.post(RECIEVE_MESSAGE).accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().is(HttpStatus.BAD_REQUEST.value()));

	}

	private static String asJsonString(final Object obj) throws Exception {
		return new ObjectMapper().writeValueAsString(obj);
	}
}
