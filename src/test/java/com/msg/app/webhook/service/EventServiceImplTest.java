package com.msg.app.webhook.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import java.util.Arrays;
import java.util.Optional;
import java.util.UUID;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.amqp.AmqpException;
import org.springframework.http.HttpStatus;

import com.msg.app.conversation.bean.ConversationEntity;
import com.msg.app.conversation.dao.ConversationRepository;
import com.msg.app.queue.QueueSender;
import com.msg.app.webhook.bean.Body;
import com.msg.app.webhook.bean.Changes;
import com.msg.app.webhook.bean.Event;
import com.msg.app.webhook.bean.OriginatorMetadata;
import com.msg.app.webhook.bean.ResponseContent;
import com.msg.app.webhook.service.EventServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class EventServiceImplTest {

	@Mock
	private QueueSender queueSender;

	@Mock
	private ConversationRepository conversationRepository;

	@InjectMocks
	private EventServiceImpl eventServiceImpl;

	private ResponseContent createResponseContent() {
		final Event event = new Event();
		event.setContentType("text/plain");
		event.setMessage("Hello from the Agent");
		event.setType("ContentEvent");

		final OriginatorMetadata originatorMetadata = new OriginatorMetadata();
		originatorMetadata.setId("29c3019d-fe0b-5200-afe8-737abfeae4b4");
		originatorMetadata.setRole("ASSIGNED_AGENT");

		final Changes change = new Changes();
		change.setEvent(event);
		change.setOriginatorMetadata(originatorMetadata);
		change.setDialogId("5491244f-e462-4c63-bf4e-faf4b2d3e7b5");
		change.setSequence(2);
		change.setOriginatorId("29c3019d-fe0b-5200-afe8-737abfeae4b4");
		change.setServerTimestamp(1528371199686L);

		final Body body = new Body();
		body.setChanges(Arrays.asList(change));

		final ResponseContent response = new ResponseContent();
		response.setBody(body);
		response.setKind("notification");
		response.setType("ms.MessagingEventNotification");

		return response;
	}

	private ConversationEntity createConversationEntity() {
		final ConversationEntity result = new ConversationEntity();
		result.setBrandEmail("supporter@mail.com");
		return result;
	}

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testSendMessageToQueueError() {
		when(conversationRepository.findById(any(UUID.class))).thenReturn(Optional.of(createConversationEntity()));
		doThrow(new AmqpException("Send message error")).when(queueSender)
				.sendMessageToQueue(any(ResponseContent.class));
		final HttpStatus result = eventServiceImpl.sendMessageToQueue(createResponseContent());
		assertThat(result.value()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR.value());
	}

	@Test
	public void testSendMessageToQueueErrorWhenNotHaveConversationInDB() {
		when(conversationRepository.findById(any(UUID.class))).thenReturn(Optional.empty());
		final HttpStatus result = eventServiceImpl.sendMessageToQueue(createResponseContent());
		assertThat(result.value()).isEqualTo(HttpStatus.BAD_REQUEST.value());
	}

	@Test
	public void testSendMessageToQueueSuccessfully() {
		when(conversationRepository.findById(any(UUID.class))).thenReturn(Optional.of(createConversationEntity()));
		doNothing().when(queueSender).sendMessageToQueue(any(ResponseContent.class));
		final HttpStatus result = eventServiceImpl.sendMessageToQueue(createResponseContent());
		assertThat(result.value()).isEqualTo(HttpStatus.OK.value());
	}
}
